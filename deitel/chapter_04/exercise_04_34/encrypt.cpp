#include <iostream>
 
int
main()
{   
    std::cout << "Enter 4 digit number: ";
    int number;
    std::cin >> number;
  
    if (number < 0) {
        std::cerr << "Error 1: Invalid number." <<std::endl;
        return 1;
    }

    if (number > 9999) {
        std::cerr << "Error 1: Invalid number." << std::endl;
         return 1;
    }
  
     int digit1 = (number / 1000 + 7) % 10;
     int digit2 = (number % 1000 / 100 + 7) % 10;
     int digit3 = (number % 100 / 10 + 7) % 10;
     int digit4 = (number % 10 + 7) % 10;
  
     std::cout << "Encrypted number is " << digit1 << digit2 << digit3 << digit4 << std::endl;
     return 0;
} 

