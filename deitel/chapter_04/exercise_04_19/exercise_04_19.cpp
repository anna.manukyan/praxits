#include <iostream>
#include <unistd.h>

int
main()
{
    int counter = 1;
    int largest1 = -2147483648;
    int largest2 = -2147483648;
    std::cout << "Input ten numbers: ";
    while (counter <= 10) {
        int number;
        std::cin >> number;
        if (number > largest1) {
            largest2 = largest1;
            largest1 = number;
        } else if (number > largest2) {
            largest2 = number;
        }
        ++counter;
    }
    std::cout << "The largest: " << largest1 << std::endl;
    std::cout << "The secnd largest: " << largest2 << std::endl;
    return 0;
}
