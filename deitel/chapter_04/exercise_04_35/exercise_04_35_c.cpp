#include <iostream>

int
main()
{
    int accuracy;
    std::cout << "Enter the accuracy e: ";
    std::cin >> accuracy;
    if (accuracy < 0) {
        std::cout << "Error 1: The number should be positive. " << std::endl;
        return 1;
    }
    int x;
    std::cout << "Input exponent: ";
    std::cin >> x;
    if (x < 0) {
        std::cout << "Error 1: The number should be positive. " << std::endl;
        return 1;
    }

    int number = 1;
    int factorial = 1;
    double e = 1;
    int exponent = 1;
    while (number < accuracy) {
        exponent *= x;
        factorial *= number;
        e += static_cast<double>(exponent) / factorial;
        ++number;
    }
    std::cout << "e ^ " << x << " is " << e << std::endl;
    return 0;
}
