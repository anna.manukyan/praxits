#include <iostream>

int
main()
{
    std::cout << "Input radius: ";
    double radius;
    std::cin >> radius;

    if (radius <= 0) {
        std::cerr << "Error 1: Radius cannot be negativ." << std::endl;
        return 1;
    }
    double pi = 3.14159;
    std::cout << "Circle diameter: " << (2 * radius) << std::endl;
    std::cout << "Circle circumference: " << (2 * pi * radius) << std::endl;
    std::cout << "Circle area: " << (pi * radius * radius) << std::endl;
    return 0;
}

