#include <iostream>

int
main()
{
    while (true) {
        float hoursWorked;
        std::cout << "Enter hours worked (-1 to end): ";
        std::cin  >> hoursWorked;
        if (static_cast<int>(hoursWorked) == -1) {
            return 0;
        }

        if (hoursWorked < 0) {
            std::cout << "Error 1: Worked hours cannot be negative." << std::endl;
            return 1;
        }

        float hourlyRate;
        std::cout << "Enter hourly rate of the worker ($00.00): ";
        std::cin  >> hourlyRate;
        if (hourlyRate < 0) {
            std::cout << "Error 2: Hourly rate cannot be negative." << std::endl;
            return 2;
        }


        float salary = hoursWorked * hourlyRate;
        if (hoursWorked > 40) {
            salary += (hoursWorked - 40) * hourlyRate / 2;
        }

        std::cout << "Salary is $" << salary << "\n" << std::endl;
    }
    return 0;    
}

