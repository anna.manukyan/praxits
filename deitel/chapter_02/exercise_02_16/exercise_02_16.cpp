#include <iostream> 

int 
main()
{
    int number1; /// first integer
    int number2; /// second integer

    std::cout << "Enter two integers: "; /// prompt user for data
    std::cin >> number1 >> number2; /// read two integers from user
 
    int sum = number1 + number2;
    std::cout << "Sum is " << sum << std::endl;
 
    int product = number1 * number2;
    std::cout << "Product is " << product << std::endl;

    int difference = number1 - number2;
    std::cout << "Difference is " << difference << std::endl;
   
    if (0 == number2) {
        std::cout << "Error 1: The second number cannot be 0" << std::endl;
        return 1;
    }

    std::cout << "Quotient is " << number1 / number2 << std::endl; 
    return 0;
} /// end function main

