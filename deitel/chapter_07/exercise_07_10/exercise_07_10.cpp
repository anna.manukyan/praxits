#include <iostream>
#include <unistd.h>
#include <stdlib.h>

void inputEmployeeSalaries(int counterArray[], const int counterSize);
void printRangeCounter(const int counterArray[], const int counterSize);

int
main()
{
    const int COUNTER_SIZE = 9;
    int counterArray[COUNTER_SIZE] = {};

    inputEmployeeSalaries(counterArray, COUNTER_SIZE);
    printRangeCounter(counterArray, COUNTER_SIZE);
    return 0;
}

void
inputEmployeeSalaries(int counterArray[], const int counterSize)
{
     for (int i = 0; i < counterSize; ++i) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Input sales: ";
        }

        int grossSale;
        std::cin >> grossSale;
        if (grossSale < 0) {
            std::cerr << "Error 1: Sales cannot be negative!" << std::endl;
            ::exit(1);
        }
        const int salary = 200 + grossSale * 0.09;
        int counter = salary / 100 - 2;
        if (counter >= 8) {
            counter = 8;
        }
        ++counterArray[counter];
    }

}

void
printRangeCounter(const int counterArray[], const int counterSize)
{
    int counter = 0;
    for (int i = 200; ; i += 100) {
        if (i >= 1000) {
            std::cout << "$1000 and more: " << counterArray[counterSize - 1] << std::endl;
            break;
        }

        std::cout << "$" << i << " - " << i + 99 << ": ";
        std::cout << counterArray[counter] << std::endl;
        ++counter;
    }
}

