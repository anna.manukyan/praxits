#include <iostream>
#include <cstring>

bool testPalindrome(const char stringArray[], int start, int end);

int
main()
{
    std::cout << "Input string: ";
    const int STRING_SIZE = 256;
    char stringArray[STRING_SIZE];
    std::cin.getline(stringArray, STRING_SIZE);
    const int stringSize = ::strlen(stringArray);
    std::cout << testPalindrome(stringArray, 0, stringSize -1);


}

bool 
testPalindrome(const char stringArray[], int start, int end)
{
    if (start >= end) {
        return true;
    }

    while ((stringArray[start] < 'a' || stringArray[start] > 'z') && (stringArray[start] < 'A' || stringArray[start] > 'Z')) {
        ++ start;
    }
    
    while ((stringArray[start] < 'a' || stringArray[start] > 'z') && (stringArray[start] < 'A' || stringArray[start] > 'Z')) {
        --end;
    }

     if (::toupper(stringArray[start]) != ::toupper(stringArray[end])) {
        return false;
    }

      return testPalindrome(stringArray, start + 1, end - 1);
}
