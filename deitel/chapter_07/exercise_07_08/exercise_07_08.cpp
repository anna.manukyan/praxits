a. Display the value of element 6 of character array f.
ANS: std::cout << f[6] << std::endl;

b. Input a value into element 4 of one-dimensional floating-point array b.
ANS: float b[10] = {};
      std::cin >> b[4];

c. Initialize each of the 5 elements of one-dimensional integer array g to 8.
ANS: int g[5] = {8, 8, 8, 8, 8};

d. Total and print the elements of floating-point array c of 100 elements.
ANS: float total = 0;
        for (int i = 0; i < 100; ++i) {
            total += c[i];
            std::cout << c[i] << std::endl;
    }

e. Copy array a into the first portion of array b. Assume double a[ 11 ], b[ 34 ];
Ans: double a[ 11 ], b[ 34 ]; 
        for (int i = 0; i < 11; ++i){
            b[i] = a[i];
        }

f. Determine and print the smallest and largest values contained in 99-element floating-point array w.
ANS:
    float maximum = w[0];
    float minimum = w[0];

    for (int i = 0; i < 99; ++i) {
        if (w[i] < minimum) {
            minimum = w[i];
        }
    
        if(w[i] > maximum) {
            maximum = w[i];
        }
    }
    std::cout << "maximum: " << maximum << std::endl;
    std::cout << "minimum: " << minimum << std::endl;
}

