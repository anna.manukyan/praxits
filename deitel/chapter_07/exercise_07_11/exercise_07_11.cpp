#include <iostream>
#include <unistd.h>

void bubbleSort(int array[], const int size);
void getArrayElementsFromUser(int array[], const int size);
void printArrayElements(const int array[], const int size);
int
main()
{ 
    const int SIZE = 10;
    int arrayOfNumbers[SIZE] = {};
    getArrayElementsFromUser(arrayOfNumbers, SIZE);
    bubbleSort(arrayOfNumbers, SIZE);
    printArrayElements(arrayOfNumbers, SIZE);

    return 0;
}

void
getArrayElementsFromUser(int array[], const int size)
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input 10 integer values for the array to store in: ";
    }
    for (int i = 0; i < size; ++i) {
        std::cin >> array[i];
    }
}

void
bubbleSort(int array[], int size)
{
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size - 1; ++j) {
            if (array[j + 1] < array[j]) {
                std::swap(array[j + 1], array[j]);
            }
        }
    }
}

void
printArrayElements(const int array[], const int size)
{
    for (int i = 0; i < size; ++i) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
}

