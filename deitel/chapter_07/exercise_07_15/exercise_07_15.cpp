#include <iostream>

bool checkingNumber(const int array[], const int counter, const int number);

int
main()
{     
    const int SIZE = 19;
    int arrayNumber[SIZE] = {};
    int counter = 0;
    std::cout << "Input twenty number: ";
    for (int i = 0; i < 20; i++) {
        int number;
        std::cin >> number;
        if (number  < 10 || number > 100) {
            std::cout << "Error1: Invalid input." << std::endl;
            return 1;
        }
        if (!checkingNumber(arrayNumber, number, i)) {
            std::cout << number << " ";
        }
        if (counter < SIZE) {
            arrayNumber[counter++] = number;
        }

    }

    std::cout << std::endl;
    return 0;

}

bool
checkingNumber(const int array[], const int number, const int counter)
{
    for (int i = 0; i < counter; ++i) {
        if (number == array[i]) {
            return true;
        }
    }
    return false;
}


