#include <iostream>
#include "Invoice.hpp"

int
main()
{

    Invoice item("abc", "washing powder", 20, 50 );
    std::cout << "\tProduct code: " << item.getPartNumber() << std::endl;
    std::cout << "\tProduct description: " << item.getPartDescription() << std::endl;
    std::cout << "\tQuantity of products sold: " << item.getItemQuantity() << std::endl;
    std::cout << "\tPrice of one item: " << item.getItemPrice() << std::endl;
    std::cout << "\tTotal invoice amount: " << item.getInvoiceAmount() << std::endl;

    return 0;
}
