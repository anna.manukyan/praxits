#include <string>

class Invoice
{
public:
    Invoice(std::string partNumber, std::string partDescription, int itemQuantity, int itemPrice);

    void setPartNumber(std::string partNumber);
    std::string getPartNumber();

    void setPartDescription(std::string partDescription);
    std::string getPartDescription();

    void setItemQuantity(int itemQuantity);
    int getItemQuantity();

    void setItemPrice(int itemPrice);
    int getItemPrice();

    int getInvoiceAmount();

private:
    std::string partNumber_;
    std::string partDescription_;
    int itemQuantity_;
    int itemPrice_;
};
