class Account
{
public:
    Account(int balance);
    int getAccountBalance();
    void credit(int credit);
    void debit(int debit);
private:
    int accountBalance_;
};
