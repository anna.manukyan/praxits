#include <iostream>
#include "Account.hpp"

int
main()
{
    Account account1(10000);
    Account account2(250);
        
    int credit1, debit1;
    std::cout << "First Account \n";
    std::cout << "Please, add an ammount /credit/: \n";
    std::cin >> credit1;
    account1.credit(credit1);
    std::cout << "Please, input an ammount to withdraw /debit/: \n";
    std::cin >> debit1;
    account1.debit(debit1);
    std::cout << "Current account balance is: " << account1.getAccountBalance() << std::endl;

    int credit2, debit2;
    std::cout << "Second Account \n";
    std::cout << "Please, add an ammount /credit/: \n";
    std::cin >> credit2;
    account2.credit(credit2);
    std::cout << "Please, input an ammount to withdraw /debit/: \n";
    std::cin >> debit2;
    account2.debit(debit2);
    std::cout << "Current account balance is: " << account2.getAccountBalance() << std::endl;
    return 0;
}

