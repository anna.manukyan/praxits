#include <iostream>  
#include <iomanip>

 
int 
main() 
{  
    /// set floating point format
    std::cout << std::fixed << std::setprecision(2);
    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << "Year" << std::setw(23) << "Deposit With rate " << rate << "%" << std::endl;
        double amount = 24.00; /// account amount at the end of each year 
        double percent =  rate / 100.0;
        for (int year = 1626; year <= 2021; ++year) {
            amount += amount * percent;
            std::cout << year << std::setw(100) << amount << "$" << std::endl;
        }           
      
     } 
 

    return 0;  
}  

