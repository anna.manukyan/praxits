#include <iostream>

int 
main()
{

    for (int count = 1; count <= 4; ++count) {
        std::cout << count << " ";  
    } 

    std::cout << "\nBroke out of loop at count = " << std::endl;
    return 0;
}

