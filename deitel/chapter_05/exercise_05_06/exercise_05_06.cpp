#include <iostream>

int
main()
{
    int sum = 0;
    int counter = 0;
    
    while (true) {
        std::cout << "Input of numbers (9999 to exit): ";
        int number;
        std::cin >> number;
        if (9999 == number) {
            break;

        }
        
        sum += number;
        ++counter;

    }
    
    if (0 == counter) {
        std::cout << "Error 1: The input numbers cannot be 0. " << std ::endl;
        return 1;
    }
    std::cout << "Average of numbers: " << sum / counter << std::endl;
    return 0;
}

