#include <iostream>

int 
main()

{
    std::cout << "Factorials of 1-5 " << std::endl;
    int factorial = 1;
    
    for (int counter = 1; counter <= 5; ++ counter) {
        factorial *= counter;
        std::cout << counter << "\t "<< factorial << " " << std::endl;   
    }

    return 0;

}

