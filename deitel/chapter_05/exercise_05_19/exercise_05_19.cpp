#include <iostream>

int
main()
{
    double piNumber = 3.14159;
    int plus = 1;
    int minus = 3;
    int counter = 1;
    double pi = 0.0;
    double epsilon = 0.000001;
    int precision = 100;
    while (precision <= 1000000) {
        pi += 4.0 / plus - 4.0 / minus;
        double delta = piNumber * precision;
        if ((static_cast<int>(delta) - pi * precision) <= epsilon) {
                precision *= 10;
                std::cout << std::fixed << pi << "\t" << counter << std::endl;
        }
        plus += 4;
        minus += 4;
        ++counter;
    }
    return 0;

}
