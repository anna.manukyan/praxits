#include <iostream>

int
main()
{
    std::cout << "Input tow numbers: ";
    int x, y;
    std::cin >> x >> y;
    
    if ((x >= 5) && (y < 7) == !((x < 5) || (y >= 7))) {
        std::cout << "!(x < 5) && !(y >= 7)  is equivalent to     !((x < 5) || (y >= 7))"  << std::endl;
    } else {
        std::cout << "!(x < 5) && !(y >= 7)  is not equivalent to !((x < 5) || (y >= 7))"  << std::endl;
    }
    
    std::cout << "\n";
    
    std::cout << "Input three numbers: ";
    int a, b, g;
    std::cin >> a >> b >> g;

    if ((a != b) || (g == 5) == !((a == b) && (g != 5))) {
        std::cout << "!(a == b) || !(g != 5) is equivalent to     !((a == b) && (g != 5))" << std::endl;
    } else {
        std::cout << "!(a == b) || !(g != 5) is not equivalent to !((a == b) && (g != 5))" << std::endl;
    }

    std::cout << "\n";

    std::cout << "Input tow numbers: ";
    std::cin >> x >> y;

    if (!((x <= 8) && (y > 4)) == (x > 8) || (y <= 4)) {
        std::cout << "!((x <= 8) && (y > 4)) is equivalent to     !(x <= 8) || !(y > 4)"   << std::endl;
    } else {
        std::cout << "!((x <= 8) && (y > 4)) is not equivalent to !(x <= 8) || !(y > 4)"   << std::endl;
    }
    
    std::cout << "\n";

    std::cout << "Input tow numbers: ";
    int i, j; 
    std::cin >> i >> j;

    if (!((i > 4) || (j <= 6)) == (i <= 4) && (j > 6)) {
        std::cout << "!((i > 4) || (j <= 6)) is equivalent to     !(i > 4) && !(j <= 6)"   << std::endl;
    } else {
        std::cout << "!((i > 4) || (j <= 6)) is not equivalent to !(i > 4) && !(j <= 6)"   << std::endl;
    }

    return 0;
}

