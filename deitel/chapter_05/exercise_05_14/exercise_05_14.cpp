#include <iostream>
#include <iomanip>

int
main()
{
    float total = 0;

    while (true) {
        std::cout << "Enter product number (1-5)(-1 to stop). ";
        int product;
        std::cin >> product;
        if (-1 == product) {
            break;
        }
        if (product < 1 || product > 5) {
            std::cerr << "Error 1: Invalid product number. " << std::endl;
            return 1;
        }
        std::cout << "Enter sold quantity: ";
        int number;
        std::cin >> number;

        if (number < 0) {
            std::cerr << "Error 2: Quantity cannot be negative" << std::endl;
            return 2;
        }
        switch (product) {
        case 1: total += number * 2.98; break;
        case 2: total += number * 4.50; break;
        case 3: total += number * 9.98; break;
        case 4: total += number * 4.49; break;
        case 5: total += number * 6.87; break;
        }
    }

    std::cout << "Total: " << std::fixed << std::setprecision(2) << total << "$" << std::endl;
    return 0;
}

