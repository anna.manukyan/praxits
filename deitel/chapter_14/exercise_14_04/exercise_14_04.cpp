#include  <iostream>
#include <unistd.h>


// function template printArray definition
template<typename T>
void
printArray(const T *array, int count)
{
    for (int i = 0; i < count; i++) {
        std::cout << array[i] << " ";
    }
    std::cout << std::endl;
} // end function template printArray

template<typename T>
int
printArray(const T *array, int size, int lowSubscript, int highSubscript)
{
    if (size < 0 || lowSubscript < 0 ||  highSubscript >= size) {
        return 0;
    }
    for (int count = lowSubscript; count <=  highSubscript; ++count) {
        std::cout << array[count] << ' ';
    }
    std::cout << std::endl;
    return highSubscript - lowSubscript + 1;
}

int
main()
{
    const int ARRAY_SIZE = 10;
    int integersArray[ARRAY_SIZE] = {};
    double doublesArray[ARRAY_SIZE] = {};
    char text[ARRAY_SIZE] = "Program";
    printArray(integersArray, ARRAY_SIZE);
    std::cout << "Original array: ";

    std::cout << printArray(integersArray, ARRAY_SIZE, 0, ARRAY_SIZE - 1) << " elements." << std::endl;

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input low subscript: ";
    }
    int lowSubscript;
    std::cin >> lowSubscript;
    if (lowSubscript < 0 || lowSubscript >= ARRAY_SIZE) {
        std::cerr << "Error 1: The low subscript cannot be negative, exceed the size of the array, or be exactly to it\a" << std::endl;
        return 1;
    }

    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input high subsctipt: ";
    }
    int highSubscript;
    std::cin >> highSubscript;
    if (highSubscript < 0 || highSubscript >= ARRAY_SIZE) {
        std::cerr << "Error 2: The high subscript cannot be negative, exceed the size of the array, or be exactly to it.\a" << std::endl;
        return 2;
    }
  
    std::cout << "Desired part of the array and number of elements (integers): ";
    std::cout << printArray(integersArray, ARRAY_SIZE, lowSubscript, highSubscript) << " elements." << std::endl;
    
    std::cout << "Desired part of the array and number of elements (doubles): ";
    std::cout << printArray(doublesArray, ARRAY_SIZE, lowSubscript, highSubscript) << " elements." << std::endl;

    std::cout << "Desired part of the array and number of elements (string): ";
    std::cout << printArray(text, ARRAY_SIZE, lowSubscript, highSubscript) << " elements." << std::endl;
    return 0;
}

