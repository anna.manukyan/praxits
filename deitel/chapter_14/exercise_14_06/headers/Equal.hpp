#ifndef __EQUAL_HPP__
#define __EQUAL_HPP__

template <typename T>
bool isEqualTo(const T& lhv, const T& rhv);

class Test;
bool operator==(const Test& lhv, const Test& rhv);

class Test
{
    friend bool operator==(const Test& lhv, const Test& rhv);
public:
    Test (const int value = 0) : data_(value) {}
private:
    int data_;
};

template <typename T>
bool
isEqualTo(const T& lhv, const T& rhv)
{
    return lhv == rhv;
}
#endif /// __EQUAL_HPP__

