#include "headers/Equal.hpp"

#include <iostream>

bool operator==(const Test& lhv, const Test& rhv) /// Only built-in objects can be compared, user-defined objects need operator overloading.
{
    return lhv.data_ == rhv.data_;
}

