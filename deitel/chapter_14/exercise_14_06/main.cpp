#include<iostream>

#include "headers/Equal.hpp"

int
main()
{
    int number1, number2;
    std::cin >> number1 >> number2;
    Test object1(number1);
    Test object2(number2);

    std::cout << "Test in two numbers: " << isEqualTo(number1, number2) << std::endl;
    std::cout << "Test in two objects: " << isEqualTo(object1, object2) << std::endl;
    return 0;
}

