#include<iostream>

#include <iomanip>

typedef unsigned int uint;

template <typename T>
void selectionSort(T* const array, const uint& size);

template <typename T>
void swap(T* const ptr1, T* const ptr2);

int
main()
{
    const uint arraySize = 10;
    int a[arraySize] = {2, 6, 4, 8, 10, 12, 89, 68, 45, 37};
    std::cout << "Int data items in original order\n";
    for (uint i = 0; i < arraySize; i++) {
        std::cout << std::setw(4) << a[i];
    }
    selectionSort<int>(a, arraySize);
    std::cout << "\nInt data items in ascending order\n";
    for (uint j = 0; j < arraySize; j++) {
        std::cout << std::setw(4) << a[j];
    }
    std::cout << std::endl;

    float b[arraySize] = {2.4, 6.0, 4.5, 8.9, 10.2, 12.6, 89.7, 68.9, 45.3, 37.7};
    std::cout << "Float data items in original order\n";
    for (uint i = 0; i < arraySize; i++) {
        std::cout << std::fixed << std::setw(6) << std::setprecision(1) << b[i];
    }
    selectionSort<float>(b, arraySize);
    std::cout << "\nFloat data items in ascending order\n";
    for (uint j = 0; j < arraySize; j++) {
        std::cout << std::fixed << std::setw(6) << std::setprecision(1) << b[j];
    }
    std::cout << std::endl;

    return 0;
}



template <typename T>
void
selectionSort(T* const array, const uint& size)
{
    for (uint i = 0; i < size - 1; ++i) {
        uint smallest = i;
        for (uint index = i + 1; index < size; ++index) {
            if (array[index] < array[smallest]) {
                smallest = index;
            }
        }
        std::swap(array[i], array[smallest]);
    }
}

template <typename T>
void
swap(T* const ptr1, T* const ptr2)
{                                                            
    T hold = *ptr1;                                  
    *ptr1 = *ptr2;                              
    *ptr2 = hold;                                      
}                                     

