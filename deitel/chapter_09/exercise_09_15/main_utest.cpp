#include "headers/Board.hpp"
#include "headers/TicTacToe.hpp"
#include "headers/Player.hpp"
#include <string>

#include <gtest/gtest.h>

TEST(Board, EmptyBoard)
{
    Board b;
    std::stringstream s;
    b.printBoard(s);
    std::string str = s.str();
    for(int i = 0; i <= 22; i+= 2) {
        EXPECT_EQ(str[i], '|');
    }

    for (int row = 0; row < Board::BOARD_SIZE; ++row) {
        for (int column = 0; column < Board::BOARD_SIZE; ++column) {
            EXPECT_EQ(str[row * 8 + column * 2 + 1], ' ');
        }
    }
}

TEST(Board, SetMark)
{
    Board b;
    EXPECT_EQ(b.setMark(0, 0, 'x'), 0);
    EXPECT_EQ(b.setMark(Board::BOARD_SIZE, Board::BOARD_SIZE, 'x'), 1);
    EXPECT_EQ(b.setMark(0, 0, 'x'), 2);
}

TEST(Board, RowWins) 
{
    
    for (int row = 0; row < Board::BOARD_SIZE; ++row) {
        Board b;
        for (int column = 0; column < Board::BOARD_SIZE; ++column) {
            b.setMark(row, column, 'x');
        }
        std::stringstream s;
        b.printBoard(s);
        const std::string& str = s.str();
        for (int column = 0; column < Board::BOARD_SIZE; ++column) {
            EXPECT_EQ(str[row * 8 + column * 2 + 1], 'x');
        }
        EXPECT_TRUE(b.isRowWin(row, 'x'));
        EXPECT_TRUE(b.isWin('x'));
    }
}
TEST(Board, ColumnWins) 
{
    
    for (int column = 0; column < Board::BOARD_SIZE; ++column) {
        Board b;
        for (int row = 0; row < Board::BOARD_SIZE; ++row) {
            b.setMark(row, column, 'x');
        }
        std::stringstream s;
        b.printBoard(s);
        const std::string& str = s.str();
        for (int row = 0; row < Board::BOARD_SIZE; ++row) {
            EXPECT_EQ(str[row * 8 + column * 2 + 1], 'x');
        }
        EXPECT_TRUE(b.isColumnWin(column, 'x'));
        EXPECT_TRUE(b.isWin('x'));
    }
}

TEST(Board, LeftDiagonalWins)
{
    Board b;
    for (int row = 0; row < Board::BOARD_SIZE; ++row) {
        b.setMark(row, row, 'x');
    }
    std::stringstream s;
    b.printBoard(s);
    const std::string& str = s.str();
    for (int row = 0; row < Board::BOARD_SIZE; ++row) {
        EXPECT_EQ(str[row * 10 + 1], 'x');
    }
    EXPECT_TRUE(b.isLeftDiagonalWin('x'));
    EXPECT_TRUE(b.isDiagonalWin('x'));
    EXPECT_TRUE(b.isWin('x'));
}

TEST(Board, RightDiagonalWins)
{
    Board b;
    for (int row = 0; row < Board::BOARD_SIZE; ++row) {
        b.setMark(row, Board::BOARD_SIZE - row - 1, 'x');
    }            
    std::stringstream s;
    b.printBoard(s);
    const std::string& str = s.str();
    for (int row = 0; row < Board::BOARD_SIZE; ++row) {
        EXPECT_EQ(str[row * 6 + 5], 'x');
    }
    EXPECT_TRUE(b.isRightDiagonalWin('x'));
    EXPECT_TRUE(b.isDiagonalWin('x'));
    EXPECT_TRUE(b.isWin('x'));
}

TEST(Player, Constructor)
{
    Player player1('x');
    Player player2('o');
    EXPECT_EQ(player1.getMark(), 'x');
    EXPECT_EQ(player2.getMark(), 'o');
}

TEST(Player, Play)
{
    Board b;
    Player player('x');
    EXPECT_EQ(player.play(&b, 0, 0), 0);
    std::stringstream s;
    b.printBoard(s);
    const std::string& str = s.str();
    EXPECT_EQ(str[1], 'x');
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}


