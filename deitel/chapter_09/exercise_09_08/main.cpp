#include "headers/Date.hpp"
#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input day: ";
    }
    int day;
    std::cin >> day;
    
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input month: ";
    }
    int month;
    std::cin >> month;
 
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input year: ";
    }
    int year;
    std::cin >> year;

  
    Date date(year, month, day);

    std::cout << "Incrementation." << std::endl;
    for (int dayCounter = 0; dayCounter < 10; ++dayCounter) {
        date.print(std::cout);
        date.nextDay();
    }
    return 0;
}

