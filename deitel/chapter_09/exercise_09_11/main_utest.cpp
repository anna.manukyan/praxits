#include "headers/Rectangle.hpp"
#include <gtest/gtest.h>

void expect_re_eq(const Rectangle& re,
                  const float length = 1, const float width = 1);


TEST(Rectangle, DefaultConstructor)
{
    const Rectangle r1;
    expect_re_eq(r1);
}

TEST(Rectangle, LengthConstructor)
{
    const Rectangle r1(3);
    expect_re_eq(r1, 3);
}

TEST(Rectangle, LengthWidthConstructor)
{
    const Rectangle r1(3, 4);
    expect_re_eq(r1, 3, 4);
}

TEST(Rectangle, setValidLength)
{
    Rectangle r1;
    r1.setLength(3);
    expect_re_eq(r1, 3);
}

TEST(Rectangle, setInvalidLength)
{
    Rectangle r1;
    r1.setLength(23);
    expect_re_eq(r1);
}

TEST(Rectangle, setValidWidth)
{
    Rectangle r1;
    r1.setWidth(4);
    expect_re_eq(r1, 1, 4);
}

TEST(Rectangle, setInvalidWidth)
{
    Rectangle r1;
    r1.setWidth(25);
    expect_re_eq(r1, 1, 1);
}

TEST(Rectangle, setLengthWidth)
{
    Rectangle r1;
    r1.setLength(10);
    r1.setWidth(11);
    expect_re_eq(r1, 10, 11);

}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

void
expect_re_eq(const Rectangle& re,
             const float length, const float width)
{
    EXPECT_EQ(re.getLength(), length);
    EXPECT_EQ(re.getWidth(), width);
    EXPECT_EQ(re.getArea(), length * width);
    EXPECT_EQ(re.getPerimeter(), 2 * (length + width));
}


