#include "headers/Rectangle.hpp"

#include <iostream>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input two sides of a rectangle: ";
    }
    float length, width;
    std::cin >> length >> width;
    Rectangle rectangle(length, width);
    std::cout << "The perimeter of rectangle is " << rectangle.getPerimeter() << std::endl;
    std::cout << "The area of rectangle is " << rectangle.getArea() << std::endl;
    
    return 0;
}

