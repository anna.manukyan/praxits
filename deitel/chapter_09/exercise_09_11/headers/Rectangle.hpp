#ifndef __RECTANGLE_HPP__
#define __RECTANGLE_HPP__

class Rectangle
{
public:
    Rectangle(const float length = 1, const float width = 1);
    
    void setLength(const float length);
    void setWidth(const float width);
    float getWidth() const;
    float getLength() const;
    float getArea() const;
    float getPerimeter() const;
    
private:
    float length_;
    float width_;
};
    
#endif /// __RECTANGLE_HPP__ 

