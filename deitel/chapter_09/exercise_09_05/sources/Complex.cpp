#include "headers/Complex.hpp"

#include <iostream>

Complex::Complex(const double real, const double imaginary)
    : real_(real)
    , imaginary_(imaginary)
{
}

Complex
Complex::plus(const Complex& rhv) const
{
    const Complex ret(getReal() + rhv.getReal(), getImaginary() + rhv.getImaginary());
    return ret;
}

Complex
Complex::minus(const Complex& rhv) const
{
    const Complex ret(getReal() - rhv.getReal(), getImaginary() - rhv.getImaginary());
    return ret;
}

Complex
Complex::multiply(const Complex& rhv) const
{
    const double real = getReal() * rhv.getReal() - getImaginary() * rhv.getImaginary();
    const double imaginary = getReal() * rhv.getImaginary() + getImaginary() * rhv.getReal();
    const Complex ret(real, imaginary);
    return ret;
}

Complex
Complex::divide(const Complex& rhv) const
{
    const double squareSum = rhv.getReal() * rhv.getReal() + rhv.getImaginary() * rhv.getImaginary();
    const double realNumerator = getReal() * rhv.getReal() + getImaginary() * rhv.getImaginary();
    const double imaginaryNumerator = getImaginary() * rhv.getReal() - getReal() * rhv.getImaginary(); 
    const Complex ret(realNumerator / squareSum, imaginaryNumerator / squareSum);
    return ret;
}

void
Complex::print() const
{
    std::cout << "(" << getReal() << " + " << getImaginary() << "i)";
}



