#include "headers/Complex.hpp"

#include <gtest/gtest.h>

TEST(Complex, plus)
{
    const Complex c1(2, 6), c2(3, 5);
    const Complex c3 = c1.plus(c2);
    EXPECT_EQ(c3.getReal(), 5);
    EXPECT_EQ(c3.getImaginary(), 11);
}

TEST(Complex, minus)
{
    const Complex c1(6, 3), c2(5, 2);
    const Complex c3 = c1.minus(c2);
    EXPECT_EQ(c3.getReal(), 1);
    EXPECT_EQ(c3.getImaginary(), 1);
}

TEST(Complex,  multiply)
{
    const Complex c1(2, 6), c2(3, 5);
    const Complex c3 = c1.multiply(c2);
    EXPECT_EQ(c3.getReal(), -24);
    EXPECT_EQ(c3.getImaginary(), 28);
}

TEST(Complex, divide)
{
    const Complex c1(6, 8), c2(3, 4);
    const Complex c3 = c1.divide(c2);
    EXPECT_EQ(c3.getReal(), 2);
    EXPECT_EQ(c3.getImaginary(), 0);
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

