#include "headers/DateAndTime.hpp"
#include "headers/errorMessage.hpp"

#include <iostream>
#include <unistd.h>
#include <string>

int
main()
{
    DateAndTime dateAndTime;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input year: ";
    }
    uint year;
    std::cin >> year;
    const uint yearErrorCode = dateAndTime.setYear(year);
    printErrorMessage(yearErrorCode);
    if (yearErrorCode > 0) {
        return yearErrorCode;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input month: ";
    }
    uint month;
    std::cin >> month;
    const uint monthErrorCode = dateAndTime.setMonth(month);
    printErrorMessage(monthErrorCode);
    if (monthErrorCode > 0) {
        return monthErrorCode;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input day: ";
    }
    uint day;
    std::cin >> day;
    const uint dayErrorCode = dateAndTime.setDay(day);
    printErrorMessage(dayErrorCode);
    if (dayErrorCode > 0) {
        return dayErrorCode;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input hour: ";
    }
    uint hour;
    std::cin >> hour;
    const uint hourErrorCode = dateAndTime.setHours(hour);
    printErrorMessage(hourErrorCode);
    if (hourErrorCode > 0) {
        return hourErrorCode;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input minute: ";
    }
    uint minute;
    std::cin >> minute;
    const uint minuteErrorCode = dateAndTime.setMinutes(minute);
    printErrorMessage(minuteErrorCode);
    if (minuteErrorCode > 0) {
        return minuteErrorCode;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Input second: ";
    }
    uint second;
    std::cin >> second;
    const uint secondErrorCode = dateAndTime.setSeconds(second);
    printErrorMessage(secondErrorCode);
    if (secondErrorCode > 0) {
        return secondErrorCode;
    }

    return 0;
}

