#include "headers/DateAndTime.hpp"
#include <gtest/gtest.h>

void expect_dt_eq(const DateAndTime& dt,
                  const uint year = 2000, const uint month = 1,  const uint day = 1,
                  const uint hour = 0,    const uint minute = 0, const uint second = 0);

TEST(DateAndTime, DefaultConstructor)
{
    const DateAndTime d1;
    expect_dt_eq(d1);
}

TEST(DateAndTime, setYearValid)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setYear(2021), 0);
    expect_dt_eq(d1, 2021);
}

TEST(DateAndTime, setYearInvalid)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setYear(1821), 1);
    expect_dt_eq(d1);
}

TEST(DateAndTime, setMonthValid)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setMonth(10), 0);
    expect_dt_eq(d1, 2000, 10);
}

TEST(DateAndTime, setMonthInvalid)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setMonth(100), 2);
    expect_dt_eq(d1);
}

TEST(DateAndTime, setDayValid)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setDay(11), 0);
    expect_dt_eq(d1, 2000, 1, 11);
}

TEST(DateAndTime, setDayInvalid)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setDay(100), 3);
    expect_dt_eq(d1);
}

TEST(DateAndTime, IsLeapYear)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setYear(2021), 0);
    EXPECT_FALSE(d1.isLeapYear());
    expect_dt_eq(d1, 2021);
}

TEST(DateAndTime, IsLeapYear400)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setYear(2000), 0);
    EXPECT_TRUE(d1.isLeapYear());
    expect_dt_eq(d1, 2000);
}

TEST(DateAndTime, IsLeapYear100)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setYear(2100), 0);
    EXPECT_FALSE(d1.isLeapYear());
    expect_dt_eq(d1, 2100);
}

TEST(DateAndTime, IsLeapYear4)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setYear(2020), 0);
    EXPECT_TRUE(d1.isLeapYear());
    expect_dt_eq(d1, 2020);
}

TEST(DateAndTime, LastDayOfMonth)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setYear(2021), 0);
    EXPECT_EQ(d1.setMonth(2), 0);
    EXPECT_EQ(d1.getLastDayOfMonth(), 28);
}

TEST(DateAndTime, LastDayOfMonthLeapYear)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setMonth(5), 0);
    EXPECT_EQ(d1.getLastDayOfMonth(), 31);
}

TEST(DateAndTime, LastDayOfMonthLeapYearFeb)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setYear(2020), 0);
    EXPECT_EQ(d1.setMonth(2), 0);
    EXPECT_EQ(d1.getLastDayOfMonth(), 29);
}

TEST(DateAndTime, CurrentTime)
{
    EXPECT_TRUE(true); /// No way to test the current time
}

TEST(DateAndTime, ValidHours)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setHours(10), 0);
    expect_dt_eq(d1, 2000, 1, 1, 10);
}

TEST(DateAndTime, InvalidHours)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setHours(25), 4);
    expect_dt_eq(d1);
}

TEST(DateAndTime, NextHours)
{
    DateAndTime d1;
    d1.nextHour();
    expect_dt_eq(d1, 2000, 1, 1, 1);
}

TEST(DateAndTime, NextDayAndHour)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setHours(23), 0);
    d1.nextHour();
    expect_dt_eq(d1, 2000, 1, 2, 0);
}

TEST(DateAndTime, ValidMinutes)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setMinutes(20), 0);
    expect_dt_eq(d1, 2000, 1, 1, 0, 20);
}

TEST(DateAndTime, InvalidMinutes)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setMinutes(65), 5);
    expect_dt_eq(d1);
}

TEST(DateAndTime, NextMinute)
{
    DateAndTime d1;
    d1.nextMinute();
    expect_dt_eq(d1, 2000, 1, 1, 0, 1);
}

TEST(DateAndTime, NextHourAndMinute)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setMinutes(59), 0);
    d1.nextMinute();
    expect_dt_eq(d1, 2000, 1, 1, 1, 0);
}

TEST(DateAndTime, ValidSeconds)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setSeconds(30), 0);
    expect_dt_eq(d1, 2000, 1, 1, 0, 0, 30);
}

TEST(DateAndTime, InvalidSeconds)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setSeconds(65), 6);
    expect_dt_eq(d1);
}

TEST(DateAndTime, NextSecond)
{
    DateAndTime d1;
    d1.nextSecond();
    expect_dt_eq(d1, 2000, 1, 1, 0, 0, 1);
}

TEST(DateAndTime, NextMinuteAndSecond)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setSeconds(59), 0);
    d1.nextSecond();
    expect_dt_eq(d1, 2000, 1, 1, 0, 1, 0);
}

TEST(DateAndTime, NextYear)
{
    DateAndTime d1;
    d1.nextYear();
    expect_dt_eq(d1, 2001);
}

TEST(DateAndTime, NextMonth)
{
    DateAndTime d1;
    d1.nextMonth();
    expect_dt_eq(d1, 2000, 2);
}

TEST(DateAndTime, NextYearAndMonth)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setMonth(12), 0);
    d1.nextMonth();
    expect_dt_eq(d1, 2001, 1);
}

TEST(DateAndTime, NextDay)
{
    DateAndTime d1;
    d1.nextDay();
    expect_dt_eq(d1, 2000, 1, 2);
}

TEST(DateAndTime, NextMonthAndDay)
{
    DateAndTime d1;
    EXPECT_EQ(d1.setDay(31), 0);
    d1.nextDay();
    expect_dt_eq(d1, 2000, 2, 1);
}

TEST(DateAndTime, iterateDate)
{
    EXPECT_TRUE(true); /// No way to test the current time
}

int
main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

void
expect_dt_eq(const DateAndTime& dt,
             const uint year, const uint month,  const uint day,
             const uint hour, const uint minute, const uint second)
{
    EXPECT_EQ(dt.getYear(), year);
    EXPECT_EQ(dt.getMonth(), month);
    EXPECT_EQ(dt.getDay(), day);
    EXPECT_EQ(dt.getHours(), hour);
    EXPECT_EQ(dt.getMinutes(), minute);
    EXPECT_EQ(dt.getSeconds(), second);
}

