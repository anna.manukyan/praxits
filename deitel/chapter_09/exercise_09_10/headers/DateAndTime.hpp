#ifndef __DATE_AND_TIME_HPP__
#define __DATE_AND_TIME_HPP__

typedef unsigned int uint;

class DateAndTime
{
public:
    DateAndTime();
    
    uint setYear(const uint year);
    uint setMonth(const uint month);
    uint setDay(const uint day);
    void setCurrentTime();
    uint setHours(const uint time);
    uint setMinutes(const uint time);
    uint setSeconds(const uint time);
    uint getYear() const;
    uint getMonth() const;
    uint getDay() const;
    uint getHours() const;
    uint getMinutes() const;
    uint getSeconds() const;
    void tick(const uint tickCount);
    void nextYear();
    void nextMonth();
    void nextDay();
    void nextHour();
    void nextMinute();
    void nextSecond();
    void iterateDate(const uint days);
    void printUniversal();
    void printStandard();
    bool isLeapYear() const;
    uint getLastDayOfMonth() const;
    
private:
    uint year_;
    uint month_;
    uint day_;
    uint hours_;
    uint minutes_;
    uint seconds_;
};

#endif /// __DATE_AND_TIME_HPP__ 

