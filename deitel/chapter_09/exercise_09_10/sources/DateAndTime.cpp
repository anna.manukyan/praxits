#include "headers/DateAndTime.hpp"

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cassert>

static const uint monthDays[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

DateAndTime::DateAndTime()
    : year_(2000)
    , month_(1)
    , day_(1)
    , hours_(0)
    , minutes_(0)
    , seconds_(0)
{
}

uint
DateAndTime::setYear(const uint year)
{
    if (year < 1900 || year > 9999) {
        return 1;
    }
    year_ = year;
    return 0;
}

uint
DateAndTime::setMonth(const uint month)
{
    if (month < 1 || month > 12) {
        return 2;
    }
    month_ = month;
    return 0;
}

uint
DateAndTime::setDay(const uint day)
{
    /// ternary operator for february 29
    if (day < 1 || day > getLastDayOfMonth()) {
        return 3;
    }
    day_ = day;
    return 0;
}

void
DateAndTime::setCurrentTime()
{
    const uint time = static_cast<uint>(std::time(0));
    setHours((time % 86400) / 3600 + 4); /// GMT +4
    setMinutes((time % 3600) / 60);
    setSeconds(time % 60);
}

uint
DateAndTime::setHours(const uint hours)
{
    if (hours > 23) {
        return 4;
    }
    hours_ = hours;
    return 0;
}

uint
DateAndTime::setMinutes(const uint minutes)
{
    if (minutes > 59) {
        return 5;
    }
    minutes_ = minutes;
    return 0;
}

uint
DateAndTime::setSeconds(const uint seconds)
{
    if (seconds > 59) {
        return 6;
    }
    seconds_ = seconds;
    return 0;
}

uint
DateAndTime::getYear() const
{
    return year_;
}

uint
DateAndTime::getMonth() const
{
    return month_;
}

uint
DateAndTime::getDay() const
{
    return day_;
}

uint
DateAndTime::getHours() const
{
    return hours_;
}

uint
DateAndTime::getMinutes() const
{
    return minutes_;
}

uint
DateAndTime::getSeconds() const
{
    return seconds_;
}

void
DateAndTime::printUniversal()
{
    std::cout << std::setfill('0') << std::setw(2) << getHours() << ":"
              << std::setw(2) << getMinutes() << ":" << std::setw(2) << getSeconds() << " "
              << std::setfill('0') << std::setw(2) << getDay() << "-"
              << std::setw(2) << getMonth() << "-" << std::setw(4)
              << getYear() << std::endl;
}

void
DateAndTime::printStandard()
{
    std::cout << std::setfill('0') << std::setw(2)
              << ((0 == getHours() || 12 == getHours()) ? 12 : getHours() % 12)
              << ":" << std::setfill('0') << std::setw(2) << getMinutes()
              << ":" << std::setw(2) << getSeconds() << (getHours() < 12 ? "AM " : "PM ")
              << std::setfill('0') << std::setw(2) << getDay() << "-"
              << std::setw(2) << getMonth() << "-" << std::setw(4)
              << getYear() << std::endl;
}

void
DateAndTime::tick(const uint tickCount)
{
    /// get the time inputed by the user in total seconds
    const uint totalSeconds = getHours() * 3600 + getMinutes() * 60 + getSeconds();
    for (uint sec = totalSeconds; sec <= totalSeconds + tickCount; ++sec) {
        nextSecond();
    }
}

void
DateAndTime::nextYear()
{
    setYear(getYear() + 1);
}

void
DateAndTime::nextMonth()
{
    const uint nextMonth = getMonth() + 1;
    if (nextMonth <= 12) { 
        setMonth(nextMonth);
    } else {
        setMonth(1);
        nextYear();
    }
}

void
DateAndTime::nextDay()
{
    const uint nextDay = getDay() + 1;
    const uint monthDaysCount = getLastDayOfMonth();
    if (nextDay <= monthDaysCount) {
        setDay(nextDay);
    } else {
        setDay(1); 
        nextMonth();
    }
}

void
DateAndTime::nextHour()
{
    const uint nextHour = getHours() + 1;
    if (nextHour <= 23) {
        setHours(nextHour);
    } else {
        setHours(0);
        nextDay();
    }
}

void
DateAndTime::nextMinute()
{
    const uint nextMinute = getMinutes() + 1;
    if (nextMinute <= 59) {
        setMinutes(nextMinute);
    } else {
        setMinutes(0);
        nextHour();
    }
}

void
DateAndTime::nextSecond()
{
    const uint nextSecond = getSeconds() + 1;
    if (nextSecond <= 59) {
        setSeconds(nextSecond);
    } else {
        setSeconds(0);
        nextMinute();
    }
}

void
DateAndTime::iterateDate(const uint days)
{
    for (uint day = 0; day <= days; ++day) {
        nextDay();
    }
}

/// setting leap year (aka february 29) for every 4 years, 100 years and 400 years
bool
DateAndTime::isLeapYear() const
{
    const uint year = getYear();
    return ((0 == year % 4 && 0 != year % 100) || (0 == year % 400)) ? true : false;
}

uint
DateAndTime::getLastDayOfMonth() const
{
    return (isLeapYear() && 2 == getMonth()) ? monthDays[getMonth()] + 1 : monthDays[getMonth()];
}

