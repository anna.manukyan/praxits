#include "headers/errorMessage.hpp"

#include <iostream>
#include <cassert>

static const char* timeElements[] = {" ", "year", "month", "day", "hour", "minute", "second"};

void
printErrorMessage(const uint errorNumber)
{
    if (0 == errorNumber) {
       return;
    } 
    assert(errorNumber < (sizeof(timeElements) / sizeof(timeElements[0])));
    std::cerr << "Error " << errorNumber << ": Wrong " << timeElements[errorNumber] << " data input!" << std::endl;
}

