#ifndef __TIME_HPP__
#define __TIME_HPP__

class Time /// class name
{
public: 
    Time();
    Time(int hour, int minute = 0, int second = 0); /// class constructor

    void setTime(int hour, int minute, int second); /// time setting 
    void getTime(); /// display the current time

private:
    int hour_;
    int minute_;
    int second_;

}; /// end of class declaration Time

#endif /// __TIME_HPP__

