#include "headers/Time.hpp"

#include <iostream>
#include <iomanip>
#include <ctime>

Time::Time()
{
    const time_t currentTime = ::time(NULL);
    const int hour = static_cast<int>(currentTime) % 86400 / 3600;
    const int minute = static_cast<int>(currentTime) % 3600 / 60;
    const int second = static_cast<int>(currentTime) % 60;
    setTime(hour, minute, second); /// time setting function call
} 

Time::Time(int hour, int minute, int second) /// class constructor
{
    setTime(hour, minute, second); /// time setting function call
}

void
Time::setTime(int hour, int minute, int second)
{ /// time setting
    hour_   = ( hour >= 0 && hour < 24 ) ? hour : 0;
    minute_ = ( minute >= 0 && minute < 60 ) ? minute : 0; 
    second_  = ( second >= 0 && second < 60 ) ? second : 0;
}

void
Time::getTime()
{ /// display the current time
  
    std::cout << "time: " << std::setfill('0')
              << std::setw(2) << hour_ << ":"
              << std::setw(2) << minute_ << ":"
              << std::setw(2) << second_ << std::endl;
}

