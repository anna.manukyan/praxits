#include <iostream>
#include <cassert>

void symmetrySquare(const int sideLength);

int
main()
{
    std::cout << "Input the side length: ";
    int sideLength;
    std::cin >> sideLength;
    if (sideLength <= 0) { 
        std::cerr << "Error 1: The side length cannot be negative or zero.\a " << std::endl;
        return 1;
    }
    symmetrySquare(sideLength);
    return 0;
}
void
symmetrySquare(const int sideLength)
{
    assert(sideLength > 0);
    for (int count = 0; count < sideLength; ++count) {
        for (int i = 0; i < sideLength; ++i) {
                std::cout << '*';
        }
        std::cout << std::endl;
    }
}
