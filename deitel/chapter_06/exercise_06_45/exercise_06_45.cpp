#include <iostream>

int gcd(const int x, const int y);

int
main()
{
    std::cout << "Input two numbers: ";
    int x;
    int y;
    std::cin >> x >> y;
    std::cout << "The greatest common divisor: " << gcd(x, y) << std::endl;
    return 0;
}

int
gcd(const int x, const int y)
{
    if (0 == y) {
        return x;
    }

    return gcd(y, x % y);
}
