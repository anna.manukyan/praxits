#include <iostream>
#include <cassert>

int quotient(const int number1, const int number2);
int reminder(const int number1, const int number2);
void printDigit(int number);

int
main()
{
    std::cout << "Input number from range 1 to 32767: ";
    int number;
    std::cin >> number;
    if (number < 1 || number > 32767) {
    	std::cerr << "Error 1: Wrong input!" << std::endl;
    	return 1;
    }
    
    std::cout << "Original number: " << number << std::endl;
    std::cout << "Divisor number: "; 
    printDigit(number);
    return 0;
}

inline int
quotient(const int number1, const int number2)
{
    assert(number2 != 0);
    const int value = number1 / number2;
    return value;
}

inline int
reminder(const int number1, const int number2)
{
    assert(number2 != 0);
    const int value = number1 % number2;
    return value;
}

inline void
printDigit(int number)
{
    assert(number >= 1 && number <= 32767);
    int divisor = 10000;
    while (divisor > number) {
        divisor = quotient(divisor, 10);
    }
    while (divisor >= 1) {
        std::cout << quotient(number, divisor) << "  ";
        number = reminder(number, divisor);
        divisor = quotient(divisor, 10);
    }
    std::cout << std::endl;
}

