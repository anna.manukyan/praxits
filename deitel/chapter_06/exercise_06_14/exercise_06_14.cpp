#include<iostream> 
#include <cmath>

double roundToInteger(const double number);
double roundToTenths(const double number);
double roundToHundredths(const double number);
double roundToThousandths(const double number);

int
main()
{
    double number = 0;
    while (true) {
        std::cout << "Enter number to round (EOF to end): ";
        std::cin >> number;
        if (std::cin.eof()) {
            break;
        }

        std::cout << "Value: " << number << std::endl;
        std::cout << "Rounded to integer: " << roundToInteger(number) << std::endl;
        std::cout << "Rounded to tenths: " << roundToTenths(number) << std::endl;
        std::cout << "Rounded to hundredths: " << roundToHundredths(number) << std::endl;
        std::cout << "Rounded to thousendths: " << roundToThousandths(number) << std::endl;
      
    }
    return 0;
}
///rounding
double
roundToInteger(const double number)
{
    return std::floor(number + .5);
}
 
double
roundToTenths(const double number)
{
    return std::floor(number * 10 + .5) / 10;
}
 
double
roundToHundredths(const double number)
{
    return std::floor(number * 100 + .5) / 100;
}
 
double
roundToThousandths(const double number)
{
    return std::floor(number * 1000 + .5) / 1000;
}
