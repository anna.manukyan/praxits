#include <iostream>
#include <cassert>

void symmetrySquare (const int sideLength, const char fillCharacter);

int
main()
{
    std::cout << "Input side length: ";
    int sideLength;
    std::cin >> sideLength;
    if (sideLength <= 0) {
        std::cerr << "Error1: Side lengt cannot be negative or zero.\a" << std::endl;
        return 1;
    }
    std::cout << "Input simbol: ";
    char fillCharacter;
    std::cin >> fillCharacter;
    symmetrySquare(sideLength, fillCharacter);
    return 0;
}

void
symmetrySquare(const int sideLength, const char fillCharacter)
{
    assert(sideLength > 0);
    for (int i = 0; i < sideLength; ++i) {
        for (int j = 0; j < sideLength; ++j) {
            std::cout << fillCharacter;
        }
        std::cout << std::endl;
    }
}

