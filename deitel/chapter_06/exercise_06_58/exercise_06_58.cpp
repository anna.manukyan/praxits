#include <iostream>

template <typename T>
T minimum(const T& number1, const T& number2);

int
main()
{
    std::cout << "Input two integers: ";
    int number1, number2;
    std::cin >> number1 >> number2;
    std::cout << "The minimum integer value is " << minimum(number1, number2) << "\n\n";

    std::cout << "Input two float numbers: ";
    float number3, number4;
    std::cin >> number3 >> number4;
    std::cout << "The minimum floting point value is " << minimum(number3, number4) << "\n\n";
}

template <typename T>
T
minimum(const T& number1, const T& number2)
{
    return number2 < number1 ? number2 : number1;
}

