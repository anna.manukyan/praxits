#include <iostream>

int timeInSeconds(const int hour, const int minute, const int second);
bool isValidTime(const int hour, const int minute, const int second);
int
main()
{
    std::cout << "Input the first time /hour, minute, second/: ";
    int hour1;
    int minute1;
    int second1;
    std::cin >> hour1 >> minute1 >> second1;
    if (!isValidTime(hour1, minute1, second1)) {
        std::cerr << "Error 1: Wrong time input." << std::endl;
        return 1;
    }
    const int firstTimeInSeconds = timeInSeconds(hour1, minute1, second1);

    std::cout << "Input the second time /hour, minute, second/: ";
    int hour2;
    int minute2;
    int second2;
    std::cin >> hour2 >> minute2 >> second2;
    if (!isValidTime(hour2, minute2, second2)) {
        std::cerr << "Error 1: Wrong time input." << std::endl;
        return 1;

    }
    const int secondTimeInSeconds = timeInSeconds(hour2, minute2, second2);
    const int timeDifference = (firstTimeInSeconds > secondTimeInSeconds) ? firstTimeInSeconds - secondTimeInSeconds : secondTimeInSeconds;
    std::cout << timeDifference << std::endl;
    return 0;
}

int
timeInSeconds(const int hour, const int minute, const int second)
{
    const int timeInSeconds = hour * 3600 + minute * 60 + second;
    return timeInSeconds;
}

inline bool
isValidTime(const int hour, const int minute, const int second)
{
    const bool validTime = (hour >= 0   && hour   <= 12) &&
                           (minute >= 0 && minute <= 59) &&
                           (second >= 0 && second <= 59);
    return validTime;
}
