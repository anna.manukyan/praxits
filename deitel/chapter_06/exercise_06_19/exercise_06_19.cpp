#include <iostream>
#include <iomanip>
#include <cmath>
#include <cassert>

double hypotenuse(const double side1, const double side2);

int
main()
{
    std::cout << "Triangle \t" << "Side1 \t" << "Side2 \t" << "Hypotenuse " << std::endl;
    std::cout << std::setw(10) << "1 \t" << std::setw(7) << "3.0 \t" << std::setw(7)
              << "4.0 \t" << std::setw(10) << hypotenuse(3.0, 4.0) << std::endl;
    std::cout << std::setw(10) << "2 \t" << std::setw(7) << "5.0 \t" << std::setw(7)
              << "12.0 \t" << std::setw(10) << hypotenuse(5.0, 12.0) << std::endl;
    std::cout << std::setw(10) << "3 \t" << std::setw(7) << "8.0 \t" << std::setw(7)
              << "15.0 \t" << std::setw(10) << hypotenuse(8.0, 15.0) << std::endl;

    return 0;
}

double
hypotenuse(const double side1, const double side2)
{
    assert(side1 > 0 && side2 > 0);
    const double hypotenuse = std::sqrt(side1 * side1 + side2 * side2);
    return hypotenuse;
}
