#include <iostream>


int
main()
{
    ::srand(::time(0));
    while(true) {
        std::cout << "I have a number between 1 and 1000. " << std::endl;
        std::cout << "Can you guess my number?" << std::endl;
        std::cout << "Please type your first guess. " << std::endl;
        const int number = 1 + ::rand() % 1000;
        while (true) {
            int guessNumber;
            std::cin >> guessNumber;
            if (guessNumber < number) {
                std::cout << "Too few. Try again: ";
                continue;
            }
            if (guessNumber > number) {
                std::cout << "Too much. Try again: ";
                continue;
            }
            std::cout << "Fine! You guessed the number! " << std::endl;
            break;
        }
        std::cout << "Want to play same more (0 - Yes or 1 - No)?: ";
        bool selectionNumber;
        std::cin >> selectionNumber;
        if (selectionNumber) {
            std::cout << "Exiting..." << std::endl;
            break;
        }
    }
    return 0;
}

