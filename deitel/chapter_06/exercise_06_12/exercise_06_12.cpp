#include <iostream>
#include <cmath>
#include <iomanip>

double calculateCharges(const double hours);

int
main()
{
    double totalCharge = 0.0;
    double totalHours = 0.0;

    std::cout << "Enter parking hours for 3 cars (1 - 24): " << std::endl;
    std::cout << "Car \t" << "Hours \t" << "Charge" << std::endl;
    for (int count = 1; count <= 3; ++count) {
        double parkingHours;
        std::cin >> parkingHours;
        if (parkingHours <= 0 || parkingHours > 24) {
            std::cerr << "Error 1: Incorrect data entry " << std::endl;
            return 1;
        }
        const double charge = calculateCharges(parkingHours);
        std::cout << std::fixed << count << std::setw(12) << std::setprecision(1)
                  << parkingHours << std::setw(9) << std::setprecision(2)
                  << charge << std::endl;
        totalHours += parkingHours;
        totalCharge += charge;
    }

    std::cout << "TOTAL " << std::setw(7) << std::setprecision(1)
              << totalHours << std::setw(9)
              << std::setprecision(2) << totalCharge << std::endl;
    return 0;
}

inline double
calculateCharges(const double hours)
{
    if (hours >= 19) {
        return 10.0;
    }
    double charge = 2;
    if (hours > 3) {
        charge += std::ceil(hours - 3.0) * 0.5;
    }
    return charge;
}

