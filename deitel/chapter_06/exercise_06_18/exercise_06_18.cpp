#include <iostream>
#include <unistd.h>

int integerPower(int base, const unsigned int exponent);

int
main()
{
    int base;
    std::cout << "Input base number: ";
    std::cin >> base;
    std::cout << "Input exponent number: ";
    
    unsigned int exponent;
    std::cin >> exponent;
    std::cout << base << " ^ " << exponent << " = " << integerPower(base, exponent) << std::endl;
    return 0;
}
inline int
integerPower(int base, unsigned int exponent)
{
    unsigned int result = 1;
    for (int count = exponent; count > 0; count /= 2) {
        if (1 == (count & 1)) {
            result *= base;
        }
        base *= base;
    }
    return result;
}

