#include <iostream>

double minimum(const double number1, const double number2, const double number3);

int
main()
{
    std::cout << "Inpute three numbers: ";
    double number1, number2, number3;
    std::cin >> number1 >> number2 >> number3;
    std::cout << "Minimum: " << minimum(number1, number2, number3) << std::endl;
    return 0;
}

double
minimum(const double number1, const double number2, const double number3)
{
    double minimum = number1;
    if (number2 < minimum) {
        minimum = number2;
    }
    if (number3 < minimum) {
        minimum = number3;
    }
    return minimum;
}
