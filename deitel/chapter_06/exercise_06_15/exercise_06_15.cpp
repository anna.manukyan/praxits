a. What does it mean to choose numbers "at random?"
Ans: Choosing numbers at rabdom means each number from a given set or range of numbers has the equal probability of getting selected which means one is completely randomly picking out a nuview the full answer.

b. Why is the rand function useful for simulating games of chance?
Ans:It is useful because you can call on it to generate any number range where any number in the range has an equal or fair chance of coming up in play.

c. Why would you randomize a program by using srand? Under what circumstances is it desirable not to randomize?
Ans:In fact, rand() uses an algorithm and so the integers it returns are completly predictable. If you know the algorithm and the previous value returned, the next value can easily be calculated. But for many purposes the sequence of numbers that rand() produces are scrambled up enough that they can be used as random numbers.

Because they are predictable, the numbers are called pseudo-random. The function srand() initializes rand().

d. Why is it often necessary to scale or shift the values produced by rand?
 ans:  Allows for safe testing of hypothesis and can broaden the possibilities of the real-life situations by allowing for quicker discovery.
e. Why is computerized simulation of real-world situations a useful technique?
Ans:Simulation modeling solves real-world problems safely and efficiently. It provides an important method of analysis which is easily verified, communicated, and understood.
Unlike physical modeling, such as making a scale copy of a building, simulation modeling is computer based and uses algorithms and equations.

