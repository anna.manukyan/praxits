#include <iostream>

void multiplicationTable();

int
main()
{   
    ::srand(::time(0));
    multiplicationTable();
    std::cout << "Exiting..." << std::endl;
    return 0;
}

void
multiplicationTable()
{
    while (true) {
        const int number1 = ::rand() % 10;
        const int number2 = ::rand() % 10;
        while (true) {
            std::cout << "How much will be: " << number1 << "*" << number2;
            std::cout << "\nAnswer: ";
            int answer;
            std::cin >> answer;
            if(number1 * number2 == answer) {
                std::cout << "The answer is correct!" << std::endl;
                break;
            }
            std::cout << "The answer is wrong, try again" << std::endl; 
        }

        std::cout << "Do you want to contineue? (0 - Yes; 1 - No): ";
        bool number;
        std::cin >> number;
        if (number) {
            break;
        }
    }
}

