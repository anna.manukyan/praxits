#include <iostream>
#include <cmath>

double roundNumber(const double number);

int 
main()
{
    while (true) {
        std::cout << "Enter number to round (EOF to end): ";
        double number = 0.0; 
        std::cin >> number;
        if (std::cin.eof()) {
            break;
        }        
                 
        std::cout << "Original is: " << number << std::endl; 
        std::cout << "Rounded is: "  << roundNumber(number) << std::endl;
    }
    return 0;
}

/// rounding
double
roundNumber(const double number)
{
    return std::floor(number + .5);
}

