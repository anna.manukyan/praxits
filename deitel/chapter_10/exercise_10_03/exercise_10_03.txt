10.3 Compare and contrast dynamic memory allocation and deallocation operators new, new [], delete and delete [].

ANS:
Using the <new> operation, we take an additional memory from the dynamic memory and create a pointer pointing to this element. The size of the memory depends on the type of variable.
Using the <new[]> operation, we take extra memory for the array from heap memory and create a pointer pointing to this first element of the array. The memory size depends on the type of the variable and the size of the array.
Using the <delete> operation we free the occupied space from the memory.
Using the <delete[]> operation, free the space occupied for the array from memory.

