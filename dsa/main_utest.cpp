#include "headers/Vector.hpp"
#include "headers/SingleList.hpp"
#include "headers/List.hpp"
#include "headers/algo.hpp"

#include <gtest/gtest.h>

TEST(VectorTest, DefaultConstSize)
{
    cd01::Vector<int> v;
    EXPECT_EQ(v.size(), 0);
}

TEST(VectorTest, SizeConstSize)
{
    cd01::Vector<int> v(5);
    EXPECT_EQ(v.size(), 5);
}

TEST(VectorTest, OutputStream)
{
    cd01::Vector<int> v1(5), v2(3, 3);
    std::stringstream s;
    std::string str;
    s << v1 << ' ' << v2 << std::endl;
    std::getline(s, str);
    EXPECT_STREQ(str.c_str(), "( 0 0 0 0 0 ) ( 3 3 3 )");
}

TEST(VectorTest, InputStream)
{
    cd01::Vector<int> v(5);
    std::stringstream s;
    s << "( 1 2 3 4 5 )";
    s >> v;
    for (size_t i = 0; i < 5; ++i) {
        EXPECT_EQ(v[i], i + 1);
    }
}

TEST(VectorTest, LessThanOperator)
{
    const cd01::Vector<int> v1(5), v2(40);
    const cd01::Vector<int> v3(5, 6), v4(5, 7);
    EXPECT_TRUE(v1 < v2);
    EXPECT_TRUE(v3 < v4);
}

TEST(VectorTest, EqualityOperator)
{
    const cd01::Vector<int> v1(5), v2(40);
    const cd01::Vector<int> v3(5), v4(5, 7);
    EXPECT_TRUE(v1 != v2);
    EXPECT_TRUE(v3 != v4);
    EXPECT_TRUE(v1 == v3);
}

TEST(VectorTest, CopyConstructor)
{
    const cd01::Vector<int> v1(5, 7);
    cd01::Vector<int> v2 = v1;
    cd01::Vector<int> v3(v1);
    EXPECT_TRUE(v1 == v2);
    EXPECT_TRUE(v1 == v3);
}

TEST(VectorTest, AssignmentOperator)
{
    const cd01::Vector<int> v1(5, 7);
    cd01::Vector<int> v2(4, 3);
    v2 = v1;
    EXPECT_TRUE(v1 == v2);
}

TEST(VectorTest, ReserveInt)
{
    cd01::Vector<int> v(2, 2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(10);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(5);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }
}

void*
operator new(const size_t n)
{
    ///std::cout << "::operator new(" << n << ")" << std::endl;
    return ::malloc(n);
};

void*
operator new[](const size_t n)
{
    ///std::cout << "::operator new[](" << n << ")" << std::endl;
    return ::malloc(n);
};

struct A
{
    A() {
        ///std::cout << "A::A()" << std::endl;
    }
    ~A() {
        ///std::cout << "A::~A()" << std::endl;
    }
    bool operator==(const A&) const { return true; }
    static void* operator new(const size_t n) {
        ///std::cout << "A::operator new(" << n << ")" << std::endl;
        return ::operator new(n);
    }
    static void* operator new[](const size_t n) {
        ///std::cout << "A::operator new[](" << n << ")" << std::endl;
        return ::operator new[](n);
    }
};

TEST(VectorTest, ReserveA)
{
    cd01::Vector<A> v(2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    
    v.reserve(10);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);

    v.reserve(5);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);

    v.reserve(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 2);
}

TEST(VectorTest, ResizeInt)
{
    cd01::Vector<int> v(2, 2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.resize(1);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 1);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.resize(5, 5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }

    v.resize(15, 15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 15);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }
    for (size_t i = 5; i < 15; ++i) {
        EXPECT_EQ(v[i], 15);
    }
}

TEST(VectorTest, ResizeA)
{
    cd01::Vector<A> v(2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(1);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 1);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 15);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 5; i < 15; ++i) {
        EXPECT_EQ(v[i], A());
    }
}

TEST(VectorTest, Iterators)
{
    typedef cd01::Vector<int> C;
    typedef C::const_iterator CCI;

    const C c(2, 1);
    for (CCI it = c.begin(); it != c.end(); ++it) {
        EXPECT_TRUE(*it == 1);
    }
}

TEST(SingleListTest, Iterators)
{
    typedef cd01::SingleList<int> C;
    typedef C::const_iterator CCI;

    const C c(2, 1);
    for (CCI it = c.begin(); it != c.end(); ++it) {
        EXPECT_TRUE(*it == 1);
    }
}

TEST(ListTest, Iterators)
{
    typedef cd01::List<int> C;
    typedef C::const_iterator CCI;

    const C c(2, 1);
    for (CCI it = c.begin(); it != c.end(); ++it) {
        EXPECT_TRUE(*it == 1);
    }
}

TEST(AlgoTest, ListFill)
{
/*    typedef cd01::List<int> C;
    typedef C::iterator CI;

    C c(5, 1);
    for (CI it = c.begin(); it != c.end(); ++it) {
        EXPECT_TRUE(*it == 1);
    }

    fill(c.begin(), c.end(), 2);
    for (CI it = c.begin(); it != c.end(); ++it) {
        EXPECT_TRUE(*it == 1);
    }*/
}

TEST(SingleListTest, DefaultConstSize)
{
    cd01::SingleList<int> v;
    EXPECT_EQ(v.size(), 0);
}


int
main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

