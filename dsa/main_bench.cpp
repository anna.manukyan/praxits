#include "headers/Vector.hpp"

#include <benchmark/benchmark.h>
#include <vector>

static const int LOOP_SIZE = 1<<10;

static void
BM_push_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        cd01::Vector<int> v;
        for (int i = 0; i < LOOP_SIZE; ++i) {
            v.push_back(i);
        }
    }
    state.SetItemsProcessed(state.iterations() * LOOP_SIZE);
}

static void
BM_std_push_back(benchmark::State& state)
{
    while (state.KeepRunning()) {
        std::vector<int> v;
        for (int i = 0; i < LOOP_SIZE; ++i) {
            v.push_back(i);
        }
    }
    state.SetItemsProcessed(state.iterations() * LOOP_SIZE);
}

static void
BM_push_back_with_reserve(benchmark::State& state)
{
    while (state.KeepRunning()) {
        cd01::Vector<int> v;
        v.reserve(LOOP_SIZE);
        for (int i = 0; i < LOOP_SIZE; ++i) {
            v.push_back(i);
        }
    }
    state.SetItemsProcessed(state.iterations() * LOOP_SIZE);
}

static void
BM_std_push_back_with_reserve(benchmark::State& state)
{
    while (state.KeepRunning()) {
        cd01::Vector<int> v;
        v.reserve(LOOP_SIZE);
        for (int i = 0; i < LOOP_SIZE; ++i) {
            v.push_back(i);
        }
    }
    state.SetItemsProcessed(state.iterations() * LOOP_SIZE);
}

BENCHMARK(BM_push_back);
BENCHMARK(BM_std_push_back);
BENCHMARK(BM_push_back_with_reserve);
BENCHMARK(BM_std_push_back_with_reserve);

BENCHMARK_MAIN();

