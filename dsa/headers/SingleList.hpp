#ifndef __T_SINGLE_LIST_HPP__
#define __T_SINGLE_LIST_HPP__

#include <stddef.h>
#include <iostream>

namespace cd01 {
    template <typename T> class SingleList;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const cd01::SingleList<T>& rhv);
template <typename T>
std::istream& operator>>(std::istream& in, cd01::SingleList<T>& rhv);

namespace cd01 {

template <typename T>
class SingleList
{
    template <typename Type>
    friend std::ostream& ::operator<<(std::ostream& out, const SingleList<Type>& rhv);
    template <typename Type>
    friend std::istream& ::operator>>(std::istream& in, SingleList<Type>& rhv);

private:
    struct Node {
        T data_;
        Node* next_;
    };

public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef Node* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

public:
    class const_iterator {
        friend SingleList<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type& operator->() const;
        const_iterator operator++();
        const_iterator operator++(int);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
    private:
        explicit const_iterator(const pointer ptr);
    private:
        pointer ptr_;
    };

    class iterator : public const_iterator {
    };

public:
    SingleList();
    SingleList(const size_type size);
    SingleList(const size_type size, const T& initialValue);
    SingleList(const SingleList<T>& rhv);
    ~SingleList();
    size_type size() const;
    bool empty() const;
    size_type capacity() const;
    T& operator[](const size_type index);
    T operator[](const size_type index) const;
    const SingleList<T>& operator=(const SingleList<T>& rhv);
    bool operator<(const SingleList<T>& rhv) const;
    bool operator==(const SingleList<T>& rhv) const;
    bool operator!=(const SingleList<T>& rhv) const;
    void reserve(const size_type n);
    void resize(const size_type n);
    void resize(const size_type n, const T& init);
    void push_back(const T& element);
    void pop_back();
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
private:
    Node* begin_;
    Node* end_;
}; /// end of template class SingleList

} /// end of namespace cd01

#include "sources/SingleList.cpp"

#endif /// __T_SINGLE_LIST_HPP__

