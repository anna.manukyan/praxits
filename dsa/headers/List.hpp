#ifndef __T_LIST_HPP__
#define __T_LIST_HPP__

#include <stddef.h>
#include <iostream>

namespace cd01 {
    template <typename T> class List;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const cd01::List<T>& rhv);
template <typename T>
std::istream& operator>>(std::istream& in, cd01::List<T>& rhv);

namespace cd01 {

template <typename T>
class List
{
    template <typename Type>
    friend std::ostream& ::operator<<(std::ostream& out, const List<Type>& rhv);
    template <typename Type>
    friend std::istream& ::operator>>(std::istream& in, List<Type>& rhv);
private:
    struct Node {
        T data_;
        Node* next_;
        Node* prev_;
    };

public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef Node* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    class const_iterator {
        friend List<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type& operator->() const;
        const_iterator operator++();
        const_iterator operator++(int);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
    private:
        explicit const_iterator(const pointer ptr);
    private:
        pointer ptr_;
    };

    class iterator : public const_iterator {
    };

public:
    List();
    List(const size_type size);
    List(const size_type size, const T& initialValue);
    List(const List<T>& rhv);
    ~List();
    size_type size() const;
    bool empty() const;
    size_type capacity() const;
    T& operator[](const size_type index);
    T operator[](const size_type index) const;
    const List<T>& operator=(const List<T>& rhv);
    bool operator<(const List<T>& rhv) const;
    bool operator==(const List<T>& rhv) const;
    bool operator!=(const List<T>& rhv) const;
    void resize(const size_type n);
    void resize(const size_type n, const T& init);
    void push_back(const T& element);
    void pop_back();
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
private:
    Node* begin_;
    Node* end_;
}; /// end of template class List

} /// end of namespace cd01

#include "sources/List.cpp"

#endif /// __T_LIST_HPP__

