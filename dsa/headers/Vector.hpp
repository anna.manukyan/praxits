#ifndef __T_VECTOR_HPP__
#define __T_VECTOR_HPP__

#include <stddef.h>
#include <iostream>

namespace cd01 {
    template <typename T> class Vector;
}

template <typename T>
std::ostream& operator<<(std::ostream& out, const cd01::Vector<T>& rhv);
template <typename T>
std::istream& operator>>(std::istream& in, cd01::Vector<T>& rhv);

namespace cd01 {

template <typename T>
class Vector
{
    template <typename Type>
    friend std::ostream& ::operator<<(std::ostream& out, const Vector<Type>& rhv);
    template <typename Type>
    friend std::istream& ::operator>>(std::istream& in, Vector<Type>& rhv);
public:
    typedef T value_type;
    typedef value_type& reference;
    typedef const value_type& const_reference;
    typedef value_type* pointer;
    typedef std::ptrdiff_t difference_type;
    typedef std::size_t size_type;

    class const_iterator {
        friend Vector<T>;
    public:
        const_iterator();
        const_iterator(const const_iterator& rhv);
        const const_iterator& operator=(const const_iterator& rhv);
        const value_type& operator*() const;
        const value_type& operator->() const;
        const_iterator operator++();
        const_iterator operator++(int);
        bool operator==(const const_iterator& rhv) const;
        bool operator!=(const const_iterator& rhv) const;
    private:
        explicit const_iterator(const pointer ptr);
    private:
        pointer ptr_;
    };

    class iterator : public const_iterator {
    };

public:
    Vector();
    Vector(const size_type size);
    Vector(const size_type size, const T& initialValue);
    Vector(const Vector<T>& rhv);
    ~Vector();
    size_type size() const;
    bool empty() const;
    size_type capacity() const;
    T& operator[](const size_type index);
    T operator[](const size_type index) const;
    const Vector<T>& operator=(const Vector<T>& rhv);
    bool operator<(const Vector<T>& rhv) const;
    bool operator==(const Vector<T>& rhv) const;
    bool operator!=(const Vector<T>& rhv) const;
    void reserve(const size_type n);
    void resize(const size_type n);
    void resize(const size_type n, const T& init);
    void push_back(const T& element);
    void pop_back();
    const_iterator begin() const;
    iterator begin();
    const_iterator end() const;
    iterator end();
private:
    T* begin_;
    T* end_;
    T* bufferEnd_;
}; /// end of template class Vector

} /// end of namespace cd01

#include "sources/Vector.cpp"

#endif /// __T_VECTOR_HPP__

