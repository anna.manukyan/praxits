#ifndef __ALGO_HPP__
#define __ALGO_HPP__

template <typename ForwardIterator, typename T>
void
fill(ForwardIterator first, ForwardIterator last, const T& val)
{
    for ( ; first != last; ++first) {
        *first = val;
    }
}

#endif /// __ALGO_HPP__
