#include "headers/List.hpp"

#include <cassert>
#include <cstdlib>
#include <iostream>

template <typename T>
std::ostream&
operator<<(std::ostream& out, const cd01::List<T>& rhv)
{
    out << "( ";
    for (T* ptr = rhv.begin_; ptr != rhv.end_; ++ptr) {
        out << *ptr << ' ';
    }
    out << ')';
    return out;
}

template <typename T>
std::istream&
operator>>(std::istream& in, cd01::List<T>& rhv)
{
    const char obr = static_cast<char>(in.get());
    if (obr != '(' && obr != '{' && obr != '[') {
        in.setstate(std::ios_base::failbit);
        return in;
    }

    for (T* ptr = rhv.begin_; ptr != rhv.end_; ++ptr) {
        in >> *ptr;
    }

    const char cbr = static_cast<char>(in.get());
    if (cbr != ')' && cbr != '}' && cbr != ']') {
        in.setstate(std::ios_base::failbit);
        return in;
    }
    return in;
}

namespace cd01 {

template <typename T>
List<T>::List()
    : begin_(NULL)
    , end_(NULL)
{
}

template <typename T>
List<T>::List(const size_type /*size*/)
    : begin_(NULL)
    , end_(NULL)
{
    ///resize(size);
}

template <typename T>
List<T>::List(const size_type /*size*/, const T& /*initialValue*/)
    : begin_(NULL)
    , end_(NULL)
{
    ///resize(size, initialValue);
}

template <typename T>
List<T>::List(const List<T>& /*rhv*/)
    : begin_(NULL)
    , end_(NULL)
{
    /*resize(rhv.size());
    for (T* ptr1 = begin_, *ptr2 = rhv.begin_; ptr1 != end_; ++ptr1, ++ptr2) {
        *ptr1 = *ptr2;
    }*/
}

template <typename T>
List<T>::~List()
{
    ///resize(0);
    if (begin_ != NULL) {
        begin_ = end_ = NULL;
    }
}

template <typename T>
const List<T>&
List<T>::operator=(const List<T>& rhv)
{
    if (&rhv == this) {
        return *this;
    }

    if (size() != rhv.size()) {
        resize(rhv.size());
    }
    for (size_type i = 0; i < size(); ++i) {
        (*this)[i] = rhv[i];
    }
    return *this;
}

template <typename T>
T&
List<T>::operator[](const size_type index)
{
    assert(begin_ + index < end_ && "Invalid index");
    return *(begin_ + index);
}

template <typename T>
T
List<T>::operator[](const size_type index) const
{
    assert(begin_ + index < end_ && "Invalid index");
    return *(begin_ + index);
}

template <typename T>
typename List<T>::size_type
List<T>::size() const
{
    return end_ - begin_;
}

template <typename T>
bool
List<T>::empty() const
{
    return end_ == begin_;
}

template <typename T>
bool
List<T>::operator==(const List<T>& rhv) const
{
    if (size() != rhv.size()) {
        return false;
    }
    for (size_type i = 0; i < size(); ++i) {
        if ((*this)[i] != rhv[i]) {
            return false;
        }
    }
    return true;
}

template <typename T>
bool
List<T>::operator!=(const List<T>& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
bool
List<T>::operator<(const List<T>& rhv) const
{
    if (size() < rhv.size()) {
        return true;
    }
    if (size() > rhv.size()) {
        return false;
    }
    for (size_type i = 0; i < size(); ++i) {
        if ((*this)[i] < rhv[i]) {
            return true;
        }
    }
    return false;
}

template <typename T>
void
List<T>::resize(const size_type n)
{
    if (n == size()) {
        return;
    }
    if (n < size()) {
        while (end_ != begin_ + n) {
           (--end_)->~T();
        }
    }
    if (n > capacity()) {
        ///reserve(n);
    }
    if (n > size()) {
        for (; end_ != begin_ + n; ++end_) {
           *end_ = T();
        }
    }
}

template <typename T>
void
List<T>::resize(const size_type n, const T& init)
{
    if (n == size()) {
        return;
    }
    if (n < size()) {
        while (end_ != begin_ + n) {
           (--end_)->~T();
        }
    }
    if (n > capacity()) {
        ///reserve(n);
    }
    if (n > size()) {
        for (; end_ != begin_ + n; ++end_) {
           *end_ = init;
        }
    }
}

template <typename T>
void
List<T>::push_back(const T& init)
{
}

template <typename T>
void
List<T>::pop_back()
{
}

template <typename T>
typename List<T>::const_iterator
List<T>::begin() const
{
    return const_iterator(begin_);
}

template <typename T>
typename List<T>::const_iterator
List<T>::end() const
{
    return const_iterator(end_);
}

/// List<T>::const_iterator

template <typename T>
List<T>::const_iterator::const_iterator(const pointer ptr)
    : ptr_(ptr)
{
}

template <typename T>
const typename List<T>::value_type&
List<T>::const_iterator::operator*() const
{
    assert(ptr_ != NULL);
    return ptr_->data_;
}

template <typename T>
bool
List<T>::const_iterator::operator==(const typename List<T>::const_iterator& rhv) const
{
    return ptr_ == rhv.ptr_;
}

template <typename T>
bool
List<T>::const_iterator::operator!=(const typename List<T>::const_iterator& rhv) const
{
    return !(*this == rhv);
}

template <typename T>
typename List<T>::const_iterator
List<T>::const_iterator::operator++()
{
    return const_iterator(ptr_ = ptr_->next_);
}

} /// end of namespace co01

