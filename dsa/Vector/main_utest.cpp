#include "headers/Vector.hpp"

#include <gtest/gtest.h>

TEST(VectorTest, DefaultConstSize)
{
    cd01::Vector<int> v;
    EXPECT_EQ(v.size(), 0);
}

TEST(VectorTest, SizeConstSize)
{
    cd01::Vector<int> v(5);
    EXPECT_EQ(v.size(), 5);
}

TEST(VectorTest, OutputStream)
{
    cd01::Vector<int> v1(5), v2(3, 3);
    std::stringstream s;
    std::string str;
    s << v1 << ' ' << v2 << std::endl;
    std::getline(s, str);
    EXPECT_STREQ(str.c_str(), "( 0 0 0 0 0 ) ( 3 3 3 )");
}

TEST(VectorTest, InputStream)
{
    cd01::Vector<int> v(5);
    std::stringstream s;
    s << "( 1 2 3 4 5 )";
    s >> v;
    for (size_t i = 0; i < 5; ++i) {
        EXPECT_EQ(v[i], i + 1);
    }
}

TEST(VectorTest, LessThanOperator)
{
    const cd01::Vector<int> v1(5), v2(40);
    const cd01::Vector<int> v3(5, 6), v4(5, 7);
    EXPECT_TRUE(v1 < v2);
    EXPECT_TRUE(v3 < v4);
}

TEST(VectorTest, EqualityOperator)
{
    const cd01::Vector<int> v1(5), v2(40);
    const cd01::Vector<int> v3(5), v4(5, 7);
    EXPECT_TRUE(v1 != v2);
    EXPECT_TRUE(v3 != v4);
    EXPECT_TRUE(v1 == v3);
}

TEST(VectorTest, CopyConstructor)
{
    const cd01::Vector<int> v1(5, 7);
    cd01::Vector<int> v2 = v1;
    cd01::Vector<int> v3(v1);
    EXPECT_TRUE(v1 == v2);
    EXPECT_TRUE(v1 == v3);
}

TEST(VectorTest, AssignmentOperator)
{
    const cd01::Vector<int> v1(5, 7);
    cd01::Vector<int> v2(4, 3);
    v2 = v1;
    EXPECT_TRUE(v1 == v2);
}

TEST(VectorTest, ReserveInt)
{
    cd01::Vector<int> v(2, 2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(10);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(5);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.reserve(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }
}

void*
operator new(const size_t n)
{
    ///std::cout << "::operator new(" << n << ")" << std::endl;
    return ::malloc(n);
};

void*
operator new[](const size_t n)
{
    ///std::cout << "::operator new[](" << n << ")" << std::endl;
    return ::malloc(n);
};

struct A
{
    A() : data_(0) {
        ///std::cout << "A::A()" << std::endl;
    }
    A(const int data) : data_(data) {
        ///std::cout << "A::A()" << std::endl;
    }
    ~A() {
        ///std::cout << "A::~A()" << std::endl;
    }

    bool operator==(const A&) const { return true; }

    static void* operator new(const size_t n) {
        ///std::cout << "A::operator new(" << n << ")" << std::endl;
        return ::operator new(n);
    }
    static void* operator new[](const size_t n) {
        ///std::cout << "A::operator new[](" << n << ")" << std::endl;
        return ::operator new[](n);
    }

    int
    getData() const
    {
        return data_;
    }
private:
    int data_;
};

TEST(VectorTest, ReserveA)
{
    cd01::Vector<A> v(2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    
    v.reserve(10);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);

    v.reserve(5);
    EXPECT_EQ(v.capacity(), 10);
    EXPECT_EQ(v.size(), 2);

    v.reserve(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 2);
}

TEST(VectorTest, ResizeInt)
{
    cd01::Vector<int> v(2, 2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.resize(1);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 1);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }

    v.resize(5, 5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }

    v.resize(15, 15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 15);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], 2);
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], 5);
    }
    for (size_t i = 5; i < 15; ++i) {
        EXPECT_EQ(v[i], 15);
    }
}

TEST(VectorTest, ResizeA)
{
    cd01::Vector<A> v(2);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 2);
    for (size_t i = 0; i < 2; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(1);
    EXPECT_EQ(v.capacity(), 2);
    EXPECT_EQ(v.size(), 1);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(5);
    EXPECT_EQ(v.capacity(), 5);
    EXPECT_EQ(v.size(), 5);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }

    v.resize(15);
    EXPECT_EQ(v.capacity(), 15);
    EXPECT_EQ(v.size(), 15);
    for (size_t i = 0; i < 1; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 1; i < 5; ++i) {
        EXPECT_EQ(v[i], A());
    }
    for (size_t i = 5; i < 15; ++i) {
        EXPECT_EQ(v[i], A());
    }
}

TEST(VectorTest, ConstIterator)
{
    typedef cd01::Vector<int> V;
    typedef cd01::Vector<A> VA;
    typedef V::const_iterator VI;
    typedef VA::const_iterator VAI;

    const V v(2, 5); /// vector creation.
    VI it1 = v.begin(); /// copy constructor.
    VI it2; /// default constructor.
    it2 = it1; /// assignment operation.
    EXPECT_TRUE(*it2 == 5); /// equality check.

    const VA a(2, A(7));
    VAI it3 = a.begin();
    EXPECT_TRUE(it3->getData() == 7); /// calling a class function through a pointer.
    
    for ( ; it2 != v.end(); ++it2) { /// preikrement and inequality condition. 
        EXPECT_TRUE(*it2 == 5); /// equality check.
    }

    for (it2 = v.end() - 1; it2 != v.begin(); --it2) { /// predekriment and inequality condition.
        EXPECT_TRUE(*it2 == 5); /// equality check.
    }
}

TEST(VectorTest, Iterators)
{
    typedef cd01::Vector<int> V;
    typedef cd01::Vector<A> VA;
    typedef V::iterator VI;
    typedef VA::iterator VAI;

    V v(2, 5); /// vector creation.
    VI it1 = v.begin(); /// copy constructor.
    VI it2; /// default constructor.
    it2 = it1; /// assignment operation.
    EXPECT_TRUE(*it2 == 5); /// equality check.

    VA a(2, A(7));
    VAI it3 = a.begin();
    EXPECT_TRUE(it3->getData() == 7); /// calling a class function through a pointer.
    
    for ( ; it2 != v.end(); ++it2) { /// preikrement and inequality condition. 
        EXPECT_TRUE(*it2 == 5); /// equality check.
    }

    for (it2 = v.end() - 1; it2 != v.begin(); --it2) { /// predekriment and inequality condition.
        EXPECT_EQ(*it2, 5); /// equality check.
    }
 
    for ( ; it2 != v.end(); it2++) { /// postikrement and inequality condition. 
        EXPECT_TRUE(*it2 == 5); /// equality check.
    }

    for (it2 = v.end() - 1; it2 != v.begin(); it2--) { /// postdekriment and inequality condition.
        EXPECT_EQ(*it2, 5); /// equality check.
    }

}

TEST(VectorTest, Const_Reverse_Iterators)
{
    const typedef cd01::Vector<int> V;
    const typedef cd01::Vector<A> VA;
    typedef V::const_reverse_iterator VI;
    typedef VA::const_reverse_iterator VRI;

    const V v(2, 1);
    for (VI tmp = v.rbegin(); tmp != v.rend(); ++tmp) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (VI tmp = v.rend() - 1; tmp != v.rbegin() + 1; --tmp) {
        EXPECT_TRUE(*tmp == 1);
    }

    for (VI tmp = v.rbegin(); tmp != v.rend(); tmp++) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (VI tmp = v.rend() - 1; tmp != v.rbegin(); tmp--) {
        EXPECT_TRUE(*tmp == 1);
    }


    const cd01::Vector<A> a(2, A(7));
    VRI tmp = a.rbegin();
    EXPECT_TRUE(tmp->getData() == 7);
}

TEST(VectorTest, Reverse_Iterators)
{
    typedef cd01::Vector<int> V;
    typedef V::reverse_iterator VI;

    V v(2, 1);
    for (VI tmp = v.rbegin(); tmp != v.rend(); ++tmp) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (VI tmp = v.rend(); tmp != v.rbegin(); --tmp) {
        EXPECT_TRUE(*tmp == 1);
    }

    for (VI tmp = v.rbegin(); tmp != v.rend(); tmp++) {
        EXPECT_TRUE(*tmp == 1);
    }
  
    for (VI tmp = v.rend(); tmp != v.rbegin(); tmp--) {
        EXPECT_TRUE(*tmp == 1);
    }


    typedef cd01::Vector<A> VA;
    typedef VA::reverse_iterator VRI;
    cd01::Vector<A> a(2, A(7));
    VRI tmp = a.rbegin();
    EXPECT_TRUE(tmp->getData() == 7);
}

TEST(VectorTest, Insert)
{
    typedef cd01::Vector<int> V;
    V v(5,5);

    V::iterator it = v.begin();
    it += 2;

    it = v.insert(it, 100);
    EXPECT_EQ(*it, 100);
    EXPECT_EQ(v.size(), 6);

    for (size_t i = 0; i < v.size(); ++i) {
        const int value = (i == 2) ? 100 : 5;
        EXPECT_EQ(v[i], value);
    }
}

TEST(VectorTest, Erase)
{
    typedef cd01::Vector<int> V;
    V v(5);

    int arg = 1;
    for (V::iterator it = v.begin(); it < v.end(); ++it) {
        *it = arg++;
    }
    
    V::iterator it = v.begin();
    it += 2;
    v.erase(it);
    EXPECT_EQ(v.size(), 4);

    for (size_t i = 0; i < v.size(); ++i) {
        const int value = (i >= 2) ? i + 2 : i + 1;
        EXPECT_EQ(v[i], value);
    }
}

TEST(VectorTest, InsertTwo)
{
    typedef cd01::Vector<int> V;
    V v(5,5);

    V::iterator it = v.begin() + 2;

    it = v.insert(it, 7, 5);
    EXPECT_EQ(v.size(), 12);
    EXPECT_TRUE(it == v.begin() + 2);

    for (size_t i = 0; i < v.size(); ++i) {
        EXPECT_EQ(v[i], 5);
    }
}

TEST(VectorTest, InsertThree)
{
    typedef cd01::Vector<int> V;
    
    V v1(10, 5);
    V v2(10, 2);

    V::iterator it1 = v1.begin();
    V::iterator it2 = v2.begin();

    v2.insert(it2 + 3, it1, it1 + 3);
    EXPECT_TRUE(v2.size() == 13);

    size_t quantity = 0;
    for (V::iterator i = v2.begin(); i != v2.end(); ++i) {
         if (*i == 5) {
            ++quantity;
         }
    }
    EXPECT_EQ(quantity, 3);
}

TEST(VectorTest, EraseTwo)
{
    typedef cd01::Vector<int> V;
    
    V v(10, 5);
    V::iterator it = v.begin();
    v.erase(it, it + 4);
    EXPECT_EQ(v.size(), 6);

    for (size_t i = 0; i != v.size(); ++i) {
        EXPECT_EQ(v[i], 5);
    }
}

TEST(VectorTest, RangeConstructor)
{
    typedef cd01::Vector<int> V;
    V v1(10, 2);

    V::iterator it = v1.begin();

    V v2(it, it + 5);

    EXPECT_EQ(v2.size(), 5);

    for (size_t i = 0; i != v2.size(); ++i) {
        EXPECT_EQ(v2[i], 2);
    }
}

int
main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

