from urllib.parse import quote
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as BS
import re

HEADERS = {'User-Agent':'Mozilla/5.0'}
URL = "https://www.spyur.am"

class Spyur:
    def __init__(self, url):
        self.url = url

    def get_home_xml(self, tag_name):
        req = Request(self.url, headers=HEADERS)
        xml = urlopen(req).read()
        xmlobj = BS(xml, 'lxml')
        pages_urls_list = []
        urls_list = xmlobj.find_all(tag_name) 
        for url in urls_list:
            pages_urls_list.append(f"{url.text}\n")  
        return pages_urls_list 
        
    def write_firms_urls_file(self, file_name, urls_list):
        file = open(file_name, 'w', encoding="utf-8")
        for line in urls_list:
            file.write(line)   

    def read_firms_urls_file(self, file_name, lng):
        reading_file = open(file_name, 'r', encoding="utf-8")
        lines_list = []
        for line in reading_file: 
            pattern = (f'https://www.spyur.am/{lng}/.*')  
            p = re.compile(pattern) 
            if p.match(line):
                lines_list.append(line)
        return lines_list

    def get_html(self, company_url):

        company_url = str(company_url.strip())
        try:
            req = Request(company_url, headers=HEADERS)
            html = urlopen(req).read()
        except UnicodeEncodeError:
            if "alpha" in company_url:
                main_url, none_ascii_character = company_url.split("alpha")
                main_url += "alpha"
                url = main_url + quote(none_ascii_character) 
                req = Request(url, headers=HEADERS)
                html = urlopen(req).read()
                soup = BS(html, "html.parser") 
                companies = soup.find_all('div', id='results_list_wrapper')
                companies_links = []

                for company in companies:
                    companies_links.append(URL + company.find("a").get("href"))
                print(*companies_links)

                
    def get_elements(self, html, *tag_name):
        pass
    
                            
xml_url_obj = Spyur("https://www.spyur.am/sitemap/company_home_dir/company_home/company_home.xml")
firms_page_urls_list = xml_url_obj.get_home_xml("loc")
write_firms_urls = xml_url_obj.write_firms_urls_file("firms_pages_urls_list.txt", firms_page_urls_list)  
am_firms_urls_list = xml_url_obj.read_firms_urls_file("firms_pages_urls_list.txt", "am")
xml_url_obj.write_firms_urls_file("am_firms_pages_urls.txt", am_firms_urls_list)
#ru_firms_urls_list = xml_url_obj.read_firms_urls_file("firms_pages_urls_list.txt", "ru")
#xml_url_obj.write_firms_urls_file("ru_firms_pages_urls.txt", ru_firms_urls_list)
#en_firms_urls_list = xml_url_obj.read_firms_urls_file("firms_pages_urls_list.txt", "en")
#xml_url_obj.write_firms_urls_file("en_firms_pages_urls.txt", en_firms_urls_list)

#am_all_companies_list = []
for firm_url in am_firms_urls_list:
    company_url = xml_url_obj.get_html(firm_url)  
#    am_all_companies_list.append(URL + str(company_url[0]))
#    print(URL + str(company_url[0])) 



