#include <iostream>
#include <cassert>
#include <unistd.h>

#include "MiniComputer.hpp"

MiniComputer::MiniComputer()
{
    programName_ = "Mini Computer";
    version_ = "Version 1.0";
}

int
MiniComputer::run()
{
    /// Print program name and version
    /// Print main menu
    /// Get command from user
    /// Execute the command
    /// Start again from main menu
    printProgramNameAndVersion();
    
    int command = -1;
    while (command != 0) {
        printMainMenu();
        command = getCommandFromUser();
        executeCommand(command);
    }

    return 0;
}

void
MiniComputer::printProgramNameAndVersion()
{
    std::cout << programName_ << " - " << version_ << std::endl;
}

void
MiniComputer::printMainMenu()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Main Menu\n";
        for (int itemCount = 0; itemCount < 5; ++itemCount) {
            printMainMenuItem(itemCount);
        }
        std::cout << "Command: ";
    }
}

void
MiniComputer::printMainMenuItem(int index)
{
    std::cout << "\t" << index << " - " ;
    switch (index) {
    case 0: std::cout << "Exit";  break;
    case 1: std::cout << "Load";  break;
    case 2: std::cout << "Store"; break;
    case 3: std::cout << "Print"; break;
    case 4: std::cout << "Add"; break;
    default:
        std::cerr << "Assert 1: There are 5 main menu items." << std::endl;
        assert(0);
    }
    std::cout << "\n";
}

void
MiniComputer::printPrintMenu()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Print Menu\n";
        for (int itemCount = 0; itemCount < 2; ++itemCount) {
            printPrintMenuItem(itemCount);
        }
        std::cout << "Print: ";
    }
}

void
MiniComputer::printPrintMenuItem(int index)
{
    std::cout << "\t";
    switch (index) {
    case 0: std::cout << "Register"; break;
    case 1: std::cout << "String";   break;
    default:
        std::cerr << "Assert 2: There are 2 print menu items." << std::endl;
        assert(0);
    }
    std::cout << ": " << index << "\n";
}

int
MiniComputer::getCommandFromUser()
{
    int command;
    std::cin >> command;
    return command;
}

void
MiniComputer::executeCommand(int command)
{
    switch (command) {
    case 0: executeExitCommand();  break;
    case 1: executeLoadCommand();  break;
    case 2: executePrintCommand(); break;
    default:
        std::cout << "Error 1: Invalid command" << std::endl;
        ::exit(1);
    }
}

void
MiniComputer::executeExitCommand()
{
    std::cout << "Exiting..." << std::endl;
}

void
MiniComputer::executeLoadCommand()
{
    ::abort();
}

void
MiniComputer::executePrintCommand()
{
    printPrintMenu();
    int print = getCommandFromUser();
    (0 == print) ? executePrintRegister() : executePrintString();
}

void
MiniComputer::executePrintRegister()
{
}


void
MiniComputer::executePrintString()
{
    assert(0);
}

