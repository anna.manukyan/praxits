ONE_ACRES = 43560 

length = float(input("Enter the length of the field in feet: ")) 
width = float(input("Enter the width of the field in feet: ")) 

# Compute the area in acres 
acres = length * width / ONE_ACRES 

# Display the result 
print("The area of the field is", format(acres, ".1f"), "acres.") 
