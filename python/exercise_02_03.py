#Print a triangl lik th on blow.

# a.Using on statmnt with on print function.
print("*" + "\n" + "*" * 2 + "\n" + "*" * 3 + "\n" + "*" * 4 + "\n")

# b.Using four statmnt with on print function.
print("*", "*" * 2, "*" * 3, "*" * 4 + "\n", sep = '\n')

# c.Using four print function.
print ("*") 
print ("*" * 2)
print ("*" * 3)
print ("*" * 4)