number1, number2, number3 = map(int, input("Input three different integers: ").split())
total = number1 + number2 + number3
print("Sum is ", total)
average = total / 3
print('Average is %0.0f' % average)
product = number1 * number2 * number3
print("Product is", product)

smallest = number1
if number2 < smallest:
    smallest = number2
if number3 < smallest:
    smallest = number3
print("Smallest is", smallest)

largest = number1
if number2 > largest:
    largest = number2
if number3 > largest:
    largest = number3
print("Largest is", largest)
