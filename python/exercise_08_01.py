try:
    kilograms = float(input("Enter weight in Kg to Convert into pounds: "))
    if kilograms <= 0:
        raise ValueError("Weight can not be negative or 0: ")
except ValueError as err:
        print(err)
        exit()
pounds = kilograms * 2.2
print(kilograms, "Kilograms =",  format(pounds, ".1f"), "Pounds")
