def check_password(password):
    has_upper   = False
    has_lower   = False
    has_number  = False

   # Check each character in the password and see which requirement it meets
    for character in password:
        if character >= "A" and character <=  "Z":
            has_upper = True
        elif character >= "a" and character <= "z":
            has_lower = True
        elif character >=  "0" and character <= "9":
            has_number  = True
    if len(password) >= 8 and has_upper and has_lower and has_number:
        return    True
    return    False

def main():
    password = input ("Enter a password: ")
    if check_password(password):
        print("That's a good password.")
    else:
        print("That isn't a good password.")
