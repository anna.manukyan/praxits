# Given the algebraic equation a * x3 + 7, which of the following, if any,
#  are correct Python statements for this equation?

# a. y = a * x * x * x + 7       true
# b. y = a * x * x * (x * 7)     false
# c. y = (a * x) * x * (x +7)    false
# d. y = (a * x) * x * x + 7     true
# e. y = a * (x * x * x) + 7     true
# f. y = a * x *(x *x + 7)       false

 