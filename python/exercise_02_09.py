meal = float(input("Enter meal amount: "))
tip_percent = int(input("Enter the tip percent: "))
tip = (meal * tip_percent) / 100
total = meal + tip
print(f"Your tip was {tip:.1f}$.")
print(f"Your total bill was {total:.1f}$.")
