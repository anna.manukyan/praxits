#Այս վարժությունում դուք կստեղծեք NextPrime անունով ֆունկցիա, որը գտնում և վերադարձնում է
#առաջին պարզ թիվն ավելի մեծ է, քան որոշ ամբողջ թիվ, n. n-ի արժեքը կփոխանցվի
#գործառույթը որպես նրա միակ պարամետր: Ներառեք հիմնական ծրագիր, որը կարդում է ամբողջ թիվ
#օգտագործողը և ցուցադրում է մուտքագրված արժեքից մեծ առաջին պարզ թիվը: Ներմուծում
#և այս վարժությունն ավարտելիս օգտագործեք 8-րդ վարժության լուծումը:
def Next_Prime(n):
    while True:
        n += 1
        for i in range(2, 4):
            if n % i == 0:
                break
            else:
                return n

# simpy.nextprime()