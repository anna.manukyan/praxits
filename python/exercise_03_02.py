small_container = int(input("How many container that are 1 liter or less?: "))

large_container = int(input("How many containers that are more than 1 liter?: "))
SMALL_DEPOSIT = 0.10
LARGE_DEPOSIT = 0.25

refund = (small_container * SMALL_DEPOSIT) + (large_container * LARGE_DEPOSIT)

print("Your  total refund will be $" + "{0:.2f}".format(float(refund)))