meal = float(input("Enter the cost of your meal "))
LOCAL_TAX = .5
tax_amount = LOCAL_TAX * meal
TIP = .18
tip_amount = TIP * meal
total_amount = meal + (tip_amount) + (tax_amount)
print('Tax amount: %0.2f' % tax_amount, "$")
print('Tip amount: %0.2f' %  tip_amount, "$")
print('Total amount due is: %0.2f' % total_amount, "$")
