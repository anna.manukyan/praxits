def main():
    distance_traveled = int(input("The distance traveled by the customer (in KM): "))
    taxi_fare = calculateTaxiFare(distance_traveled)
    print(f"The taxi fare is: ${taxi_fare}")

def calculateTaxiFare(distance_traveled):
    return 4.00 + (((distance_traveled*1000) // 140) * 0.25)
