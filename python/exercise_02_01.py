
# Print a box like the one below.
# a. Using one statement with one print function.
print(("*" * 19 + "\n") * 4)


# b.Using four statement with one print function

print( "*" * 19, "*" * 19, "*" * 19, "*" * 19 + "\n", sep = '\n')

# c.Using four print function.

print ("*" * 19) 
print ("*" * 19)
print ("*" * 19)
print ("*" * 19)