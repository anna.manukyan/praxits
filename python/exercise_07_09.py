def console_center(string, width):
    spaces = " " * int((width / 2) - (len(string)/2))
    return spaces + string
 
print(console_center("Hi there", 20))
