radius = int(input("Input a radius: "))
PI = 3.14159
print(f'Diameter is {format(2 * PI, ".2f")}.')
print(f'Circumference is {format(2 * PI * radius, ".2f")}.')
print(f'Area is {format(PI * radius * radius, ".2f")}.')
