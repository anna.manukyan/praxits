# Print a box like the one below.
# a.Using one statement with one print function.

print("*" * 19 + "\n" + ("*" + " " * 17 + "*" + "\n") * 2 + "*" * 19)

# b.Using four statement with one print function.

print( "*" * 19, "*" + " " * 17 + "*", "*" + " " * 17 + "*", "*" * 19 + "\n", sep = '\n')

# c.Using four print function.

print ("*" * 19) 
print ("*" + " " * 17 + "*")
print ("*" + " " * 17 + "*")
print ("*" * 19)

