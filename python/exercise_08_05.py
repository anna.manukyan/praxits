try:
    length = float(input("Enter the length of the room in meters: ")) 
    width = float(input("Enter the width of the room in meters: ")) 
    area = length * width
    if length <= 0 and width <= 0:
        print("The number can not be negative or zero.")
except ValueError: 
    print('The area of the room is %0.0f' % area, "meters.")


