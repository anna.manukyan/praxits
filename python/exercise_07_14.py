from random import randint
SHORTEST = 7
LONGEST = 10
MIN_ASCII = 33
MAX_ASCII = 126

def random_password():
    random_length = randint(SHORTEST, LONGEST)
    result  = ""
    for i in range(random_length):
        random_char = chr(randint(MIN_ASCII, MAX_ASCII))
        result  = result + random_char
    return result
print("Your random password is:", random_password())
