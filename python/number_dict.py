user_input = int(input("Enter a number 1-9: "))
number_dict = {1 :'one', 2 :'two', 3 :'three', 4 :'four', 5 :'five', 6 :'six', 7 :'seven', 8 :'eight', 9 :'nine'}

#(1)for keys, values in number_dict.items():
#    if keys == user_input:
#       print(values)
#(2)print(number_dict.get(user_input))
print(number_dict.setdefault(user_input))
