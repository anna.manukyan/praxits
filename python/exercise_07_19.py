import math
def calculate_area(radius):
    return math.pi * radius ** 2

radius = int(input("Input circle radius: "))
print("Area of circle is =", format(calculate_area(radius), ".1f"))
