import math

taple1 = (4, 3, 2, 2, -1, 18)
print("Original Tuple: ")
print(taple1)
print("Product - multiplying all the numbers of the said tuple: ", math.prod(taple1))
taple2 = (2, 4, 8, 8, 3, 2, 9)
print("Original Tuple: ")
print(taple2)
print("Product - multiplying all the numbers of the said tuple: ", math.prod(taple2))