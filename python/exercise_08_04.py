try:
    radius = int(input("Input a radius: "))
    PI = 3.14159
    if radius <= 0:
        print("Radius can not be negative or 0: ")
        exit()
except ValueError as err:
    print(err)
else:
    print(f'Diameter is {format(2 * PI, ".2f")}.')
    print(f'Circumference is {format(2 * PI * radius, ".2f")}.')
    print(f'Area is {format(PI * radius * radius, ".2f")}.')
