# Write a program that computes and prints the result of   (512 - 282) / 47 * 48 + 5. It is roughly 0.1017.

result = float(512 - 282) / (47 * 48 + 5)
print("The result of (512 - 282) / 47 * 48 + 5 =", format(result,  ".4f"))

