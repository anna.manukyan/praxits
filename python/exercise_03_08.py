import sys
number1, number2 = map(int, input("Input two numbers: ").split())
if 0 == number2:
    sys.exit("Division by zero.")
if number1 % number2 == 0:
    print(number1, " is multiple of ", number2)
else:
    print(number1, " is not multiple of ", number2)
